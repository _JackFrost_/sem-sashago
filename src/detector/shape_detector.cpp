#include <detector/shape_detector.h>

using namespace std;
using namespace srrg_core;

namespace srrg_sashago {

  ShapeDetector::ShapeDetector(){
    _rows=0;
    _cols=0;
    _K = Eigen::Matrix3f::Zero();
    _initialized = false;
    _is_camera_matrix_set = false;
  }

  ShapeDetector::~ShapeDetector() {

  }

  void ShapeDetector::init(){
    if (! _is_camera_matrix_set)
      throw std::runtime_error("[BagashaDetector::init]| warining, camera matrix not set");

    _directions_image.create(_rows,_cols);
    initializePinholeDirections(_directions_image,_K);

    _points_image.create(_rows,_cols);
    _normals_image.create(_rows,_cols);
    _curvature_image.create(_rows,_cols);
    _regions_image.create(_rows, _cols);

    _point_detector.init();
    _line_detector.init();
    _plane_detector.init();
    
    _initialized = true;

	_printout = true;

#ifdef USE_SEMANTIC_GT
	_semantic_detector.initBBRegistry();
#endif
  }


  void ShapeDetector::computeRegions(){

    _regions_image = PlaneDetector::PixelType::Line;
    for(int r=1; r<_rows; ++r){
      const float* c_ptr = _curvature_image.ptr<const float>(r)+1;
      int* regions_ptr = _regions_image.ptr<int>(r)+1;
      for(int c=1; c<_cols-1; ++c, ++c_ptr,++regions_ptr){
        if(*c_ptr < _configuration.max_curvature)
          *regions_ptr = PlaneDetector::PixelType::Surfel; //init as surfel
      }
    }
  }


  // TODO avoid useless computation when not needed
  void ShapeDetector::setImages(const RawDepthImage& raw_depth_image_,
                                    const RGBImage& rgb_image_){
    convert_16UC1_to_32FC1(_depth_image, raw_depth_image_);

    //compute normal_image and points_image
    computePointsImage(_points_image,
                       _directions_image,
                       _depth_image,
                       _configuration.min_distance,
                       _configuration.max_distance);

    computeSimpleNormals(_cross_normals_image,
                         _points_image,
                         _configuration.col_gap,
                         _configuration.row_gap,
                         _configuration.max_distance);

    normalBlur(_normals_image,_cross_normals_image,_configuration.normals_blur);

    computeCurvature(_curvature_image,_normals_image);
    computeRegions();
  
    _rgb_image = rgb_image_;
    cv::cvtColor(_rgb_image,_gray_image,CV_BGR2GRAY);
  }

  void ShapeDetector::compute(Scene* scene_){
    _time_stats.detect = srrg_core::getTime();
    
    // Point Detection
    if(_configuration.detect_points) {
      _time_stats.point_detection = srrg_core::getTime();
      _point_detector.compute(scene_,                              
                              _rgb_image,
                              _points_image,
                              _normals_image);
      _time_stats.point_detection = srrg_core::getTime() - _time_stats.point_detection;
    }

    // Line Detection
    if(_configuration.detect_lines) {
      _time_stats.line_detection = srrg_core::getTime();
      _line_detector.compute(scene_,
                             _gray_image,
                             _points_image);
      _time_stats.line_detection = srrg_core::getTime() - _time_stats.line_detection;
    }

    // Plane Detection
    if(_configuration.detect_planes) {
	  size_t noPlaneIdx = scene_->entries().size();
      _time_stats.plane_detection = srrg_core::getTime();
      _plane_detector.compute(scene_,
                              _regions_image,
                              _points_image,
                              _normals_image,
                              _curvature_image);
      _time_stats.plane_detection = srrg_core::getTime() - _time_stats.plane_detection;
	  
	  // Iterate through the detected planes and add the x_img, y_img pixel
	  // coordinates of the median point, for semantic classification!
	  double fx = _K(0,0), fy = _K(1,1), cx = _K(0,2), cy = _K(1,2);
	  for (size_t cnt=noPlaneIdx; cnt<scene_->entries().size(); cnt++) {
		Vector3 pm = scene_->entries()[cnt]->matchable().point;
	  	int x_img = std::round(fx*pm(0) / pm(2) + cx);
	  	int y_img = std::round(fy*pm(1) / pm(2) + cy);
		scene_->entries()[cnt]->setSemaRep(cv::Point2i(x_img, y_img));
	  }
    }

    _time_stats.detect = srrg_core::getTime() - _time_stats.detect;

    if (_configuration.verbosity >= VerbosityLevel::Time) {
      std::cerr << _time_stats << std::endl;
    }
	
	
#ifdef USE_SEMANTICS
#ifndef USE_SEMANTIC_GT
	_sds = _semantic_detector.runBBDetector(/*31666668*/scene_->timeStamp(), _semantic_detector.getConfig().gt_file_path);
#else
	_sds = _semantic_detector.getSemaInfoForTime(scene_->timeStamp());
#endif
	scene_->setSemantics(_sds);
	scene_->assocSemantics();
	if (_printout) {
		//_printout = false;
		cout << "--->>> Semantic Detection Set Inspection: "
			<< "total detections: " << _sds.scores.size()
			<< " @time: " << scene_->timeStamp() << endl;
		if (_sds.scores.size() > 0) {
			cout << "Content found, first entry: "
				<< _sds.bbs[0] << ", "
				<< _sds.classIDs[0] << ", "
				<< _sds.scores[0] << endl;
		} else {
			cout << "CAUTION: Scene has no semantic content -"
				<< (scene_->hasValidSemantics() ? " NOT" : "") << " verified" << endl;
		}
	}
#endif
  }
} //ia end namespace srrg_bagasha
