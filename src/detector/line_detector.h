#pragma once
#include <Eigen/Geometry>
#include <Eigen/Eigenvalues>

#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/line_descriptor.hpp>

#include <srrg_types/cloud_3d.h>
#include <srrg_image_utils/depth_utils.h>

#include "types/scene.h"

namespace srrg_sashago {

  class LineDetector{
  public:

    struct Config {
      int scale;
      int num_octaves;
      float lp2t_threshold;
      float bp2d_threshold;
      int width_of_band;
      float distance_resolution;
      float angle_resolution;
      int vote_threshold;
      double min_line_length;
      double max_line_gap;

      Config() {
        scale = 2;
        num_octaves = 1;
        lp2t_threshold = 50;
        bp2d_threshold = 5e-3f;
        width_of_band = 14;
        distance_resolution = 1.0f;
        angle_resolution = 180.0f;
        vote_threshold = 150;
        min_line_length = 50;
        max_line_gap = 0;
      }
    };

    //! @brief ctor/dtor
    LineDetector();
    virtual ~LineDetector();

    //! @brief configuration
    inline const Config& config() const {return _configuration;}
    inline Config& mutableConfig() {return _configuration;}

    //! @brief initializes the things
    void init();
    
    //! @brief adds line matchable to the scene
    void compute(Scene* scene_,
                 const srrg_core::UnsignedCharImage& gray_image_,
                 const srrg_core::Float3Image& points_image_);
  protected:
    //! @brief aux functions
    bool lp2t(const int num_of_pixels);
    bool bp2d(const cv::Vec3f& pA,
              const cv::Vec3f& pB,
              const cv::Vec3f& pC);
    void computeLineRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                   const Eigen::Vector3f& direction);


    Config _configuration;
    bool _initialized;
    bool _good_lines;
    cv::Ptr<cv::line_descriptor::LSDDetector> _ls;
    cv::Ptr<cv::line_descriptor::BinaryDescriptor> _bd;
    
  public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  };
} //ia end namespace srrg_bagasha
