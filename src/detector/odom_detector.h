#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <Eigen/Geometry>

#include "types/defs.h"

namespace srrg_sashago {

	using OdometryMap =
		std::map<double,
		Isometry3,
		std::less<double,
		Eigen::aligned_allocator<std::pair<double, Isometry3> > >;

	class OdometryDetector {
		/**
		 * This class is responsible for reading an odometry file.
		 * TUM format is expected.
		 * For each line, the transform is expected to be the relative transform from
		 * the previous frame. E.g. if the movement is on a line at constant velocity
		 * and each frame is measured 0.1m, then all lines should be identical:
		 * 0.1 0 0 0 0 0 1
		 * */
		public:
			// Configuration struct
			struct OdomConfig {
			}

			// ctor/dtor (default)
			OdometryDetector() { _om = readOdomFile(); }
			~OdometryDetector() {}

			getOdomForTime(double);
		protected:
			OdometryMap _om;
			readOdomFile(string);

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
	
	}; // end class OdometryDetector

} // end namespace srrg_sashago
