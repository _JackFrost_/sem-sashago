#pragma once

#include <iostream>
#include <fstream>
#include <exception>
#include <string>
#include <sstream>
#include <map>
#include <opencv2/imgproc.hpp>
#include <Eigen/Geometry>

namespace srrg_sashago
{
	
	struct SemaDetectionSet {
		std::vector<cv::Rect> bbs;
		std::vector<float> scores;
		std::vector<int> classIDs;
		double timestamp;

		SemaDetectionSet() {}

		void pushBackData(cv::Rect bb, float score, int classID) {
			bbs.push_back(bb);
			scores.push_back(score);
			classIDs.push_back(classID);
		}
	};

	using SemaGTMap = std::map<double, SemaDetectionSet, std::less<double>, Eigen::aligned_allocator<std::pair<double, SemaDetectionSet> > >;

	class SemanticDetector {
		public:
			// Configuration for detector
			struct SemaConfig {
				double conf_thresh;
				int box_padding;
				std::string gt_file_path;

				SemaConfig() {
					conf_thresh = 0.8;
					box_padding = 0;	// Consider tuning
        				gt_file_path = "gtAnno.txt";
				}
			};

			// ctor/dtor
			SemanticDetector();
			~SemanticDetector();

			SemaConfig& mutableConfig() { return _config; }

			// Executes a detection operation, returning bounding boxes
			// In case the -DUSE_SEMANTIC_GT is set, this just
			// loads and returns the BB information available at the
			// path in the argument, otherwise it will run the detector
			// on the image found at this path instead.
			SemaDetectionSet runBBDetector(double, std::string gtPath = "");

			// Efficient alternative to older runBBDetector: multimap retrieival
			void initBBRegistry();
			SemaDetectionSet getSemaInfoForTime(double);

			inline const SemaConfig getConfig() const { return _config; }

			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
		protected:
			SemaConfig _config;
			SemaGTMap _detections;
		private:
			SemaDetectionSet loadBBFromPath(double, std::string);
	}; // class SemanticDetector


} // namespace srrg_sashago
