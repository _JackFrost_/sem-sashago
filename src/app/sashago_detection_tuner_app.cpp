#include "system/sashago_system.h"
#include "viewer/image_viewer.h"
#include "system/yaml_parser.h"

using namespace srrg_sashago;

bool recompute;
void onComputeTrackbar(int, void*){
  recompute = true;
}


int main(int argc, char** argv) {
  if (argc < 3)
    throw std::runtime_error("the app requires a configuration file\nusage: <executable> <configuation_file.yaml> <txtio_file>");
  
  std::string configuration_file(argv[1]);
  std::string dataset_file(argv[2]);

  TxtioParser* txtio_parser  = new TxtioParser();
  ShapeDetector* detector  = new ShapeDetector();
  ImageViewer* viewer = new ImageViewer();
  
  YamlParser yaml_parser;
  //bdc set the txtio parser and the detector to load and set the input config
  yaml_parser.mutableConfig().input_yaml_filename = configuration_file;
  yaml_parser.mutableConfig().txtio_parser = txtio_parser;
  yaml_parser.mutableConfig().detector = detector;
  yaml_parser.read();

  txtio_parser->mutableConfig().dataset_filename = dataset_file;
  txtio_parser->init();
  
  recompute = true;

  //bdc get detectors configurations values
  // detector stuff
  ShapeDetector::Config& detector_config = detector->mutableConfig();
  float& min_distance  = detector_config.min_distance;
  float& max_distance  = detector_config.max_distance;
  float& max_curvature = detector_config.max_curvature;
  int& col_gap         = detector_config.col_gap;
  int& row_gap         = detector_config.row_gap;
  int& normals_blur    = detector_config.normals_blur;
  // bool detect_points;
  // bool detect_lines;
  // bool detect_planes;
  
  // point detector stuff
  PointDetector::Config& point_config = detector->pointDetector().mutableConfig();
  int& point_threshold = point_config.detector_threshold_minimum;
  //int& point_threshold = point_config.threshold; // TODO missing checkbox

  // line detector stuff
  LineDetector::Config& line_config = detector->lineDetector().mutableConfig();
  int& scale                 = line_config.scale;
  int& num_octaves           = line_config.num_octaves;
  float& lp2t_threshold      = line_config.lp2t_threshold;
  float& bp2d_threshold      = line_config.bp2d_threshold;
  int& width_of_band         = line_config.width_of_band;
  float& distance_resolution = line_config.distance_resolution;
  float& angle_resolution    = line_config.angle_resolution;
  int& vote_threshold        = line_config.vote_threshold;
  double& min_line_length    = line_config.min_line_length;
  double& max_line_gap       = line_config.max_line_gap;

  // plane detector stuff
  PlaneDetector::Config& plane_config = detector->planeDetector().mutableConfig();
  int& min_cluster_points    = plane_config.min_cluster_points;
  
  //bdc create a scene to store the current detected matchables
  Scene* current_scene = new Scene();

  //bdc move to next image
  bool next_image = false;

  //bdc set window name and set it to viewer
  std::string window_name = "BagashaDetectionWindow";
  viewer->mutableConfig().window_name = window_name;

  //bdc create cv window and all the trackbars
  //    unfortunately the trackbar are made to use 'int'
  //    so, don't complain about the scaling. Skol, bart
  cv::namedWindow(window_name, cv::WINDOW_NORMAL);
  int int_min_distance = 1e3 * min_distance;
  cv::createTrackbar("min distance",window_name,&int_min_distance,100,onComputeTrackbar);
  int int_max_distance = 1e1 * max_distance;
  cv::createTrackbar("max distance",window_name,&int_max_distance,100,onComputeTrackbar);
  int int_max_curvature = 1e4 * max_curvature;
  cv::createTrackbar("max curvatur",window_name,&int_max_curvature,10000,onComputeTrackbar);
  cv::createTrackbar("column gap  ",window_name,&col_gap,10,onComputeTrackbar);
  cv::createTrackbar("row gap     ",window_name,&row_gap,10,onComputeTrackbar);
  cv::createTrackbar("normal blur ",window_name,&normals_blur,5,onComputeTrackbar);
  cv::createTrackbar("Pt threshold",window_name,&point_threshold,100,onComputeTrackbar);
  cv::createTrackbar("Ln scale    ",window_name,&scale,5,onComputeTrackbar);
  cv::createTrackbar("Ln num_octav",window_name,&num_octaves,3,onComputeTrackbar);
  int int_lp2t_threshold = lp2t_threshold;
  cv::createTrackbar("Ln lp2t_thr ",window_name,&int_lp2t_threshold,100,onComputeTrackbar);
  int int_bp2d_threshold = 1e4 * bp2d_threshold;
  cv::createTrackbar("Ln bp2d_thr ",window_name,&int_bp2d_threshold,100,onComputeTrackbar);
  cv::createTrackbar("Ln width_ban",window_name,&width_of_band,20,onComputeTrackbar);
  int int_distance_resolution = 1e1 * distance_resolution;
  cv::createTrackbar("Ln dist res ",window_name,&int_distance_resolution,100,onComputeTrackbar);
  int int_angle_resolution = angle_resolution;
  cv::createTrackbar("Ln ang res  ",window_name,&int_angle_resolution,270,onComputeTrackbar);
  cv::createTrackbar("Ln vote thr ",window_name,&vote_threshold,300,onComputeTrackbar);
  int int_min_line_length = min_line_length;
  cv::createTrackbar("Ln minLength",window_name,&int_min_line_length,100,onComputeTrackbar);
  int int_max_line_gap = 1e1 * max_line_gap;
  cv::createTrackbar("Ln max gap  ",window_name,&int_max_line_gap,50,onComputeTrackbar);
  cv::createTrackbar("Pl min clust",window_name,&min_cluster_points,5000,onComputeTrackbar);

  
  TxtioParser::ParserStatus status;
  while((status = txtio_parser->processData()) != TxtioParser::ParserStatus::Stop) {
    if(status == TxtioParser::ParserStatus::Unsync)
      continue;

    //ia check if it's the first message
    if (!detector->isCameraMatrixSet()) {
      detector->setK(txtio_parser->cameraMatrix());
      detector->setRowsAndCols(txtio_parser->imageRGB().rows, txtio_parser->imageRGB().cols);
      detector->init();

      viewer->setCameraMatrix(txtio_parser->cameraMatrix());
    }

    while(true) {
      if(recompute) {
        //bdc update non-int value
        min_distance = (float)int_min_distance / 1e3;        
        max_distance = (float)int_max_distance / 1e1;       
        max_curvature = (float)int_max_curvature / 1e4;
        lp2t_threshold = (float)int_lp2t_threshold;
        bp2d_threshold = (float)int_bp2d_threshold / 1e4;
        distance_resolution = (float)int_distance_resolution / 1e1;
        angle_resolution = (float)int_angle_resolution;
        min_line_length = (double)int_min_line_length;
        max_line_gap = (double)int_max_line_gap / 1e1;
        //bdc reset detector and clear scene
        detector->init();
        current_scene->clear();
        detector->setImages(txtio_parser->imageDepth(), txtio_parser->imageRGB());
        //bdc compute current scene
        detector->compute(current_scene);
        //bdc show detected matchables
        viewer->setCurrentScene(current_scene);
        viewer->setCurrentImage(txtio_parser->imageRGB());
        viewer->draw();
        recompute = false;
      } 
      
      unsigned char key=cv::waitKey(10);
      if(key == 32){ //SPACE (go to next image)
        recompute = true;
        break;
      } else if (key == 115) { // S, save
        yaml_parser.write();
      }

    } // while(true)
  } // txtio_parser loop


  delete txtio_parser;
  delete detector;
  delete current_scene;
  delete viewer;
  return 0;
}

