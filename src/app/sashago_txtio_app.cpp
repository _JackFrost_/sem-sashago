#include <system/sashago_system.h>

using namespace srrg_sashago;

int main(int argc, char** argv) {
  if (argc < 3)
    throw std::runtime_error("the app requires a configuration file\nusage: <executable> <configuation_file.yaml> <txtio_file>");

  std::string configuration_file(argv[1]);
  std::string dataset_file(argv[2]);
  QApplication* qt_server = 0;

  SashagoSystem system;
  system.loadConfigurationFromFile(configuration_file);

  if (system.config().use_gui) {
    qt_server = new QApplication(argc, argv);
    system.setupGUI(qt_server);
  }

  system.init();
  std::shared_ptr<std::thread> processing_t = system.processDataFromFileInThread(dataset_file);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  if (system.config().use_gui) {
    while (system.mapViewer()->isActive()) {
      system.updateGUI();
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
  }

  processing_t->join();

  if (qt_server) {
    qt_server->closeAllWindows();
    qt_server->quit();
    delete qt_server;
    qt_server = 0;
  }

  if (system.config().save_track_txtio)
    system.saveOutputTrackTxtio();
}
