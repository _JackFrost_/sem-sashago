#include "system/sashago_system.h"
#include "viewer/image_viewer.h"
#include "system/yaml_parser.h"
#include "relocalizer/relocalizer.h"

using namespace srrg_sashago;

bool recompute;
void onComputeTrackbar(int, void*){
  recompute = true;
}


int main(int argc, char** argv) {
  if (argc < 3)
    throw std::runtime_error("the app requires a configuration file\nusage: <executable> <configuation_file.yaml> <txtio_file>");
  
  std::string configuration_file(argv[1]);
  std::string dataset_file(argv[2]);

  TxtioParser* txtio_parser = new TxtioParser();
  ShapeDetector* detector   = new ShapeDetector();
  ImageViewer* viewer       = new ImageViewer();
  Relocalizer* relocalizer  = new Relocalizer();
  
  YamlParser yaml_parser;
  //bdc set the txtio parser and the detector to load and set the input config
  yaml_parser.mutableConfig().input_yaml_filename = configuration_file;
  yaml_parser.mutableConfig().txtio_parser        = txtio_parser;
  yaml_parser.mutableConfig().detector            = detector;
  yaml_parser.mutableConfig().relocalizer         = relocalizer;
  yaml_parser.read();

  txtio_parser->mutableConfig().dataset_filename  = dataset_file;
  txtio_parser->init();
  
  recompute = true;

  //bdc move to next image
  bool next_image = false;

  //bdc set window name and set it to viewer
  std::string window_name = "BagashaRelocalizerWindow";
  viewer->mutableConfig().window_name = window_name;
  viewer->setTuningMode(true);
  
  //bdc get relocalizer parameters
  Relocalizer::Config& relocalizer_config = relocalizer->mutableConfig();
  size_t& maximum_matching_distance = relocalizer_config.maximum_matching_distance;
  size_t& frame_interspace = relocalizer_config.frame_interspace;
  size_t& min_relocalization_matches = relocalizer_config.min_relocalization_matches;
  real& min_relocalization_score = relocalizer_config.min_relocalization_score;
  
  //bdc create cv window and all the trackbars
  //    unfortunately the trackbar are made to use 'int'
  //    so, don't complain about the scaling. Skol, bart
  cv::namedWindow(window_name, cv::WINDOW_NORMAL);
  int int_maximum_matching_distance = (int)maximum_matching_distance;  
  cv::createTrackbar("max match dist   ",window_name,&int_maximum_matching_distance,256,onComputeTrackbar);
  int int_frame_interspace = (int)frame_interspace;
  cv::createTrackbar("frame interspace ",window_name,&int_frame_interspace,1000,onComputeTrackbar);
  int int_min_relocalization_matches = (int)min_relocalization_matches;
  cv::createTrackbar("min reloc matches",window_name,&int_min_relocalization_matches,50,onComputeTrackbar);
  int int_min_reloc_score = 1e2 * min_relocalization_score;
  cv::createTrackbar("min reloc score  ",window_name,&int_min_reloc_score,100,onComputeTrackbar);  


  //bdc we need to store the scene as if we were tracking
  SceneVector scenes;
  //bdc we need to store the images for visualization
  std::vector<cv::Mat> imayges;
  
  //bdc yellow lemon tree
  Tree hbst_tree;

  //bdc vector of correspondences/Constraints (to draw them)
  ConstraintVector* constraints = new ConstraintVector();
  
  //bdc triggered by ESC key
  bool exit = false;
  
  TxtioParser::ParserStatus status;
  while((status = txtio_parser->processData()) != TxtioParser::ParserStatus::Stop && !exit) {
    if(status == TxtioParser::ParserStatus::Unsync)
      continue;

    //ia check if it's the first message
    if (!detector->isCameraMatrixSet()) {
      detector->setK(txtio_parser->cameraMatrix());
      detector->setRowsAndCols(txtio_parser->imageRGB().rows, txtio_parser->imageRGB().cols);
      detector->init();

      viewer->setCameraMatrix(txtio_parser->cameraMatrix());
    }


    //bdc create a scene to store the current detected matchables
    Scene* current_scene = new Scene();
    
    //bdc reset detector and clear scene
    detector->init();
    detector->setImages(txtio_parser->imageDepth(), txtio_parser->imageRGB());
    //bdc compute current scene
    detector->compute(current_scene);
    scenes.push_back(current_scene);
    cv::Mat stored_img;
    txtio_parser->imageRGB().copyTo(stored_img);
    imayges.push_back(stored_img);
    //bdc compute stuff to be adda to HBST (after the match)
    const SceneEntryVector& entries = current_scene->entries();
    const size_t num_entries = entries.size();
    // bdc hbst vector for ptrs and descriptors
    BinaryEntryVector binary_entry_vector(num_entries);    
    // bdc populate the hbst vector
    size_t k = 0;
    for(SceneEntry* entry_ptr : entries) {
      if(entry_ptr->matchable().type() != Matchable::Type::Plane) {
        binary_entry_vector[k++] = new BinaryEntry(entry_ptr,
                                                   entry_ptr->descriptor(),
                                                   current_scene->sceneID());
      } // if plane
    } // for entries
    binary_entry_vector.resize(k); 
    
    //bdc show detected matchables
    viewer->setCurrentScene(current_scene);
    viewer->setCurrentImage(txtio_parser->imageRGB());

    viewer->draw();

    while(true) {
      if(recompute) {
        //bdc update non-int value
        maximum_matching_distance = (size_t)int_maximum_matching_distance;
        frame_interspace = (size_t)int_frame_interspace;
        min_relocalization_matches = (size_t)int_min_relocalization_matches;
        min_relocalization_score = (real)int_min_reloc_score / 1e2;
        
        constraints->clear();
        viewer->setMatchingScene(NULL, constraints);

        
        //bdc skip if we do not have enough scenes yet
        if ((int)(scenes.size() - frame_interspace)  < 0) {
          std::cerr << "[BagashaRelocalizerTuning]| frame_interspace skip" << std::endl;
          recompute = false;
          continue;
        }
        //bdc 'quiry' the tree
        Tree::MatchVectorMap matches;
        std::cerr << "hbst_tree size: " << hbst_tree.size() << std::endl;
        hbst_tree.match(binary_entry_vector, matches, maximum_matching_distance);
        size_t num_matches = 0;
        size_t best_index = 0;
        real best_ratio = 0.0;
        //bdc  compute the score
        for (size_t match_idx = 0; match_idx < matches.size() - frame_interspace; ++match_idx) {
          real current_ratio = (real)matches[match_idx].size() / binary_entry_vector.size();
          if (current_ratio > best_ratio) {
            best_ratio = current_ratio;
            best_index = match_idx;
          } // end if
        } // end for    
        num_matches = matches[best_index].size();

        std::cerr << "[BagashaRelocalizerTuning]| hbst matching scene = " << best_index
                  << "\t num matches = "<< num_matches
                  << "\t similarity score = " << best_ratio << std::endl;

        //ia relocalization failure
        if (best_ratio < min_relocalization_score) {
          std::cerr << "[BagashaRelocalizerTuning]| min_relocalization_score skip" << std::endl;
          recompute = false;
          continue;
        }
        if (num_matches < min_relocalization_matches) {
          std::cerr << "[BagashaRelocalizerTuning]| min_relocalization_matches skip" << std::endl;
          recompute = false;
          continue;
        }

        // bdc given the num_matches and the distance btw this scene and the best_index one,
        // we can construct a vector of BaseSolver::Correspondences w/ the pointers
        Scene* matching_scene = scenes.at(best_index);
        std::cerr << FG_CYAN("[BagashaRelocalizerTuning]| Closure found with scene: ") << best_index << std::endl;

        k = 0;
        constraints->resize(matches[best_index].size());
        for (size_t i = 0; i < matches[best_index].size(); ++i) {
          //ia discard ambigous matches
          if (matches[best_index][i].object_references.size() > 1)
            continue;
          //ia THANKS TO DOMINIK SCHLEGEL WE HAVE A NICE LINE OF CODE - Best Award 2018
          constraints->at(k++) = Constraint(matches[best_index][i].object_references[0],
                                          matches[best_index][i].object_query);
        }
        constraints->resize(k);
        
        viewer->setMatchingScene(matching_scene, constraints);
        viewer->setMatchingImage(imayges[best_index]);
        viewer->draw();
        
        recompute = false;
      } 
      
      unsigned char key=cv::waitKey(10);
      if(key == 32){ //SPACE (go to next image)
        recompute = true;
        break;
      } else if (key == 27) { // ESC
        exit = true;
        break;
      } else if (key == 115) { // S, save
        yaml_parser.write();
      }
      
    } // while(true)
    
    //bdc the addition to the tree is done AFTER the match
    hbst_tree.add(binary_entry_vector);
    hbst_tree.train();
    
  } // txtio_parser loop


  delete txtio_parser;
  delete detector;
  for(Scene* scene : scenes)
    delete scene;
  delete relocalizer;
  delete constraints;
  delete viewer;
  return 0;
}

