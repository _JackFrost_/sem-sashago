#include <system/sashago_system.h>
#include "yaml_parser.h"

namespace srrg_sashago {

  YamlParser::YamlParser() {}
  YamlParser::~YamlParser() {}

  void YamlParser::read() {
    if(_configuration.input_yaml_filename.empty())
      throw std::runtime_error("[YamlParser::read] specify an input yaml configuration");

    //bdc at least a detector has to be provided
    if(!_configuration.detector)
      throw std::runtime_error("[YamlParser::read] by lord, I need a detector to work");
      
    _external_configuration = YAML::LoadFile(_configuration.input_yaml_filename);
    
    // TODO here put configuration name
    _configuration.configuration_name = _external_configuration["configuration_name"].as<std::string>();
    
    if(_configuration.system)
      _readSystemConfig();
    if(_configuration.txtio_parser)
      _readTxtioParserConfig();
    if(_configuration.detector)
      _readDetectorConfig();
    if(_configuration.tracker)
      _readTrackerConfig();
    if(_configuration.relocalizer)
      _readRelocalizerConfig();
    if(_configuration.loop_closer)
      _readLoopCloserConfig();

    std::cerr << "[YamlParser::read]| Loaded YAML Configuration ["
              << FG_GREEN(_configuration.configuration_name) << "]"
              << std::endl;
  }


  void YamlParser::write() {
    YAML::Emitter out;
    out << YAML::BeginMap;
    //bdc configuration name
    out << YAML::Key << "configuration_name" << YAML::Value << _configuration.configuration_name;
    if (_configuration.input_yaml_filename.empty()) {
      _writeSystemConfig(out);
      _writeTxtIOParserConfig(out);
      _writeDetectorConfig(out);
      _writeTrackerConfig(out);
      _writeRelocalizerConfig(out);
      _writeLoopCloserConfig(out);
    } else {
      //ia system
      if (_configuration.system) {
        _writeSystemConfig(out);
      } else {
      out << YAML::Key << "system"
          << YAML::Value << _external_configuration["system"];
      }
      //ia txtio
      if (_configuration.txtio_parser) {
        _writeTxtIOParserConfig(out);
      } else {
        //bdc message_topics
        out << YAML::Key << "message_topics"
            << YAML::Key << _external_configuration["message_topics"];
      }
      //bdc detector
      if(_configuration.detector) {
        _writeDetectorConfig(out);
      } else {
        out << YAML::Key << "detector"
            << YAML::Key << _external_configuration["detector"];
      }
      //ia tracker
      if (_configuration.tracker) {
        _writeTrackerConfig(out);
      } else {
        //bdc nn_correspondence_finder
        out << YAML::Key << "nn_correspondence_finder"
            << YAML::Value <<_external_configuration["nn_correspondence_finder"];

        //bdc aligner
        out << YAML::Key << "aligner"
            << YAML::Value <<_external_configuration["aligner"];

        //bdc tracker
        out << YAML::Key << "tracker"
            << YAML::Value <<_external_configuration["tracker"];
      }
      //bdc relocalizer
      if (_configuration.relocalizer) {
        _writeRelocalizerConfig(out);
      } else {
        out << YAML::Key << "relocalizer"
            << YAML::Value <<_external_configuration["relocalizer"];
      }
      //ia loop closer
      if (_configuration.loop_closer) {
        _writeLoopCloserConfig(out);
      } else {
        //bdc loop_closer
        out << YAML::Key << "loop_closer"
            << YAML::Value <<_external_configuration["loop_closer"];
      }
    }
    out << YAML::EndMap; //eof

    std::ofstream fout(_configuration.output_yaml_filename);
    fout << out.c_str();
    fout.close();

    std::cerr << "[YamlParser::write]| Written YAML Configuration ["
              << FG_GREEN(_configuration.configuration_name) << "]"
              << std::endl;
  }


  void YamlParser::_readSystemConfig() {
    SashagoSystem::Config& system_config = _configuration.system->mutableConfig();
    const std::string logging_level = _external_configuration["system"]["logging_level"].as<std::string>();
    if (logging_level == "None")
      system_config.verbosity = VerbosityLevel::None;
    else if (logging_level == "Info")
      system_config.verbosity = VerbosityLevel::Info;
    else if (logging_level == "Time")
      system_config.verbosity = VerbosityLevel::Time;
    else if (logging_level == "Debug")
      system_config.verbosity = VerbosityLevel::Debug;
    else
      throw std::runtime_error("[YamlParser::readSystemConfig]| invalid logging level");

    system_config.filename_output_track_standard = _external_configuration["system"]["filename_output_track_standard"].as<std::string>();
    system_config.filename_output_track_kitti = _external_configuration["system"]["filename_output_track_kitti"].as<std::string>();
    system_config.filename_output_track_txtio = _external_configuration["system"]["filename_output_track_txtio"].as<std::string>();
    
    system_config.use_guess = _external_configuration["system"]["use_guess"].as<bool>();

    system_config.use_gui = _external_configuration["system"]["use_gui"].as<bool>();

    system_config.save_open_loop_traj = _external_configuration["system"]["save_open_loop_traj"].as<bool>();
    system_config.save_final_traj = _external_configuration["system"]["save_final_traj"].as<bool>();
    system_config.save_graph = _external_configuration["system"]["save_graph"].as<bool>();
    system_config.save_track_kitti = _external_configuration["system"]["save_track_kitti"].as<bool>();
    system_config.save_track_tum = _external_configuration["system"]["save_track_tum"].as<bool>();
    system_config.save_track_txtio = _external_configuration["system"]["save_track_txtio"].as<bool>();
  }
  
  void YamlParser::_readTxtioParserConfig() {
    TxtioParser::Config& txtio_parser_config = _configuration.txtio_parser->mutableConfig();
    txtio_parser_config.topic_depth = _external_configuration["message_topics"]["topic_depth"].as<std::string>();
    txtio_parser_config.topic_rgb   = _external_configuration["message_topics"]["topic_rgb"].as<std::string>();

	// Depth scaling -> 5000 for TUM RGB-D and ICL-NUIM, 1000 for InteriorNet
	txtio_parser_config.depth_scale = _external_configuration["message_topics"]["depth_scale"].as<float>();
  }
  
  void YamlParser::_readDetectorConfig() {
    ShapeDetector::Config& detector_config = _configuration.detector->mutableConfig();
    detector_config.min_distance = _external_configuration["detector"]["min_distance"].as<float>();
    detector_config.max_distance = _external_configuration["detector"]["max_distance"].as<float>();
    detector_config.max_curvature = _external_configuration["detector"]["max_curvature"].as<float>();
    detector_config.col_gap = _external_configuration["detector"]["col_gap"].as<int>();
    detector_config.row_gap = _external_configuration["detector"]["col_gap"].as<int>();
    detector_config.normals_blur = _external_configuration["detector"]["normals_blur"].as<int>();
    detector_config.detect_points = _external_configuration["detector"]["detect_points"].as<bool>();
    detector_config.detect_lines = _external_configuration["detector"]["detect_lines"].as<bool>();
    detector_config.detect_planes = _external_configuration["detector"]["detect_planes"].as<bool>();

    PointDetector::Config& point_detector_config = _configuration.detector->pointDetector().mutableConfig();
    //ia point detection tuning
    point_detector_config.detector_threshold_maximum_change = _external_configuration["detector"]["point_detector_threshold_maximum_change"].as<float>();
    point_detector_config.detector_threshold_maximum = _external_configuration["detector"]["point_detector_threshold_maximum"].as<real>();
    point_detector_config.vertical_detectors = _external_configuration["detector"]["point_detector_vertical_detectors"].as<size_t>();
    point_detector_config.horizontal_detectors = _external_configuration["detector"]["point_detector_horizontal_detectors"].as<size_t>();
    point_detector_config.bin_size_pixels = _external_configuration["detector"]["point_detector_bin_size_pixels"].as<size_t>();
    point_detector_config.target_number_of_keypoints_tolerance = _external_configuration["detector"]["point_detector_target_number_of_keypoints_tolerance"].as<real>();      
    point_detector_config.nonmax_suppression = _external_configuration["detector"]["point_detector_nonmax_suppression"].as<bool>();
    point_detector_config.detector_threshold_minimum = _external_configuration["detector"]["point_detector_threshold_minimum"].as<size_t>();
    point_detector_config.descriptors_type = _external_configuration["detector"]["point_detector_descriptors_type"].as<std::string>();

    LineDetector::Config& line_detector_config = _configuration.detector->lineDetector().mutableConfig();
    //ia line detection tuning
    line_detector_config.scale = _external_configuration["detector"]["line_detector_scale"].as<int>();
    line_detector_config.num_octaves = _external_configuration["detector"]["line_detector_num_octaves"].as<int>();
    line_detector_config.lp2t_threshold = _external_configuration["detector"]["line_detector_lp2t_threshold"].as<float>();
    line_detector_config.bp2d_threshold = _external_configuration["detector"]["line_detector_bp2d_threshold"].as<float>();
    line_detector_config.width_of_band = _external_configuration["detector"]["line_detector_width_of_band"].as<int>();
    line_detector_config.distance_resolution = _external_configuration["detector"]["line_detector_distance_resolution"].as<float>();
    line_detector_config.angle_resolution = _external_configuration["detector"]["line_detector_angle_resolution"].as<float>();
    line_detector_config.vote_threshold = _external_configuration["detector"]["line_detector_vote_threshold"].as<int>();
    line_detector_config.min_line_length = _external_configuration["detector"]["line_detector_min_line_length"].as<double>();
    line_detector_config.max_line_gap = _external_configuration["detector"]["line_detector_max_line_gap"].as<double>();

    PlaneDetector::Config& plane_detector_config = _configuration.detector->planeDetector().mutableConfig();
    //ia plane detection tuning
    plane_detector_config.min_cluster_points = _external_configuration["detector"]["plane_detector_min_cluster_points"].as<int>();

	// Semantics
	SemanticDetector::SemaConfig& sema_detector_config = _configuration.detector->semanticDetector().mutableConfig();
	sema_detector_config.gt_file_path = _external_configuration["detector"]["sema_det_path"].as<std::string>();
  }

  
  void YamlParser::_readTrackerConfig() {
    //ia correspondance finder
    NNCorrespondenceFinder::Config& nn_correspondence_finder_config = _configuration.tracker->aligner()->nnCorrespondenceFinder()->mutableConfig();
    nn_correspondence_finder_config.direction_scale = _external_configuration["nn_correspondence_finder"]["direction_scale"].as<float>();
    nn_correspondence_finder_config.normal_scale = _external_configuration["nn_correspondence_finder"]["normal_scale"].as<float>();
    nn_correspondence_finder_config.leaf_range = _external_configuration["nn_correspondence_finder"]["leaf_range"].as<float>();
    nn_correspondence_finder_config.tree_max_distance = _external_configuration["nn_correspondence_finder"]["tree_max_distance"].as<float>();
    nn_correspondence_finder_config.hamming_point_threshold = _external_configuration["nn_correspondence_finder"]["hamming_point_threshold"].as<int>();
    nn_correspondence_finder_config.hamming_line_threshold = _external_configuration["nn_correspondence_finder"]["hamming_line_threshold"].as<int>();


    //ia aligner
    ShapesAligner::Config& aligner_config = _configuration.tracker->aligner()->mutableConfig();
    aligner_config.max_iterations = _external_configuration["aligner"]["max_iterations"].as<int>();
    aligner_config.use_support_weight = _external_configuration["aligner"]["use_support_weight"].as<bool>();
    aligner_config.use_direct_solver = _external_configuration["aligner"]["use_direct_solver"].as<bool>();
    
    //ia tracker
    ShapeTracker::Config& tracker_config = _configuration.tracker->mutableConfig();    
    tracker_config.point_distance_threshold = _external_configuration["tracker"]["point_distance_threshold"].as<float>();
    tracker_config.direction_distance_threshold = _external_configuration["tracker"]["direction_distance_threshold"].as<float>();
    tracker_config.scene_percentage_for_break = _external_configuration["tracker"]["scene_percentage_for_break"].as<float>();
    tracker_config.resolution = _external_configuration["tracker"]["resolution"].as<float>();
    tracker_config.age_threshold_point = _external_configuration["tracker"]["age_threshold_point"].as<int>();
    tracker_config.age_threshold_line = _external_configuration["tracker"]["age_threshold_line"].as<int>();
    tracker_config.age_threshold_plane = _external_configuration["tracker"]["age_threshold_plane"].as<int>();
    tracker_config.maximum_matching_distance = _external_configuration["tracker"]["maximum_matching_distance"].as<int>();
    tracker_config.recursive_edge_generation = _external_configuration["tracker"]["recursive_edge_generation"].as<bool>();
  }

  
  void YamlParser::_readRelocalizerConfig() {
    Relocalizer::Config& relocalizer_config = _configuration.relocalizer->mutableConfig();    
    relocalizer_config.maximum_matching_distance = _external_configuration["relocalizer"]["maximum_matching_distance"].as<int>();
    relocalizer_config.frame_interspace = _external_configuration["relocalizer"]["frame_interspace"].as<int>();
    relocalizer_config.min_relocalization_score = _external_configuration["relocalizer"]["min_relocalization_score"].as<float>();
    relocalizer_config.min_relocalization_matches = _external_configuration["relocalizer"]["min_relocalization_matches"].as<int>();
  }

  
  void YamlParser::_readLoopCloserConfig() {
    LoopCloser::Config& loop_closer_config = _configuration.loop_closer->mutableConfig();    
    loop_closer_config.num_iterations = _external_configuration["loop_closer"]["num_iterations"].as<int>();
    loop_closer_config.output_graph_filename = _external_configuration["loop_closer"]["g2o_filename"].as<std::string>();
    loop_closer_config.optimization_kernel = _external_configuration["loop_closer"]["optimization_kernel"].as<std::string>();
    loop_closer_config.use_robust_kernel = _external_configuration["loop_closer"]["use_kernel"].as<bool>();
    loop_closer_config.online_optimization = _external_configuration["loop_closer"]["online_optimization"].as<bool>();
  }

  void YamlParser::_writeSystemConfig(YAML::Emitter& out) {
    const SashagoSystem::Config& system_config = _configuration.system->config();
    std::string logging_level;
    switch(system_config.verbosity) {
    case None:
      logging_level = "None";
      break;

    case Info:
      logging_level = "Info";
      break;

    case Time:
      logging_level = "Time";
      break;

    case Debug:
      logging_level = "Debug";
      break;

    default:
      throw std::runtime_error("[YamlParser::writeSystemConfig]| invalid logging level");
    }

    out << YAML::Key << "system";
    out << YAML::BeginMap;
    out << YAML::Key << "logging_level" << YAML::Value << logging_level;
    out << YAML::Key << "filename_output_track_standard" << YAML::Value << system_config.filename_output_track_standard;
    out << YAML::Key << "filename_output_track_kitti" << YAML::Value << system_config.filename_output_track_kitti;
    out << YAML::Key << "filename_output_track_txtio" << YAML::Value << system_config.filename_output_track_txtio;
    out << YAML::Key << "use_gui" << YAML::Value << system_config.use_gui;
    out << YAML::Key << "use_guess" << YAML::Value << system_config.use_guess;
    out << YAML::EndMap;
  }


  void YamlParser::_writeDetectorConfig(YAML::Emitter& out) {
    //bdc detector
    const ShapeDetector::Config& detector_config = _configuration.detector->config();
    const float& min_distance  = detector_config.min_distance;
    const float& max_distance  = detector_config.max_distance;
    const float& max_curvature = detector_config.max_curvature;
    const int& col_gap         = detector_config.col_gap;
    const int& row_gap         = detector_config.row_gap;
    const int& normals_blur    = detector_config.normals_blur;
    const bool& detect_points  = detector_config.detect_points;
    const bool& detect_lines   = detector_config.detect_lines;
    const bool& detect_planes  = detector_config.detect_planes;
    out << YAML::Key << "detector";
    out << YAML::BeginMap;
    out << YAML::Key << "min_distance" << YAML::Value << min_distance;
    out << YAML::Key << "max_distance" << YAML::Value << max_distance;
    out << YAML::Key << "max_curvature" << YAML::Value << max_curvature;
    out << YAML::Key << "col_gap" << YAML::Value << col_gap;
    out << YAML::Key << "row_gap" << YAML::Value << row_gap;
    out << YAML::Key << "normals_blur" << YAML::Value << normals_blur;
    out << YAML::Key << "detect_points" << YAML::Value << detect_points;
    out << YAML::Key << "detect_lines" << YAML::Value << detect_lines;
    out << YAML::Key << "detect_planes" << YAML::Value << detect_planes;
    //bdc  point detector
    const PointDetector::Config& point_config = _configuration.detector->pointDetector().config();

    const size_t& detector_threshold_minimum = point_config.detector_threshold_minimum;
    const real& detector_threshold_maximum_change = point_config.detector_threshold_maximum_change;
    const size_t& detector_threshold_maximum = point_config.detector_threshold_maximum;
    const size_t& vertical_detectors = point_config.vertical_detectors;
    const size_t& horizontal_detectors = point_config.horizontal_detectors;
    const size_t& bin_size_pixels = point_config.bin_size_pixels;
    const real& target_number_of_keypoints_tolerance = point_config.target_number_of_keypoints_tolerance;
    const bool& nonmax_suppression = point_config.nonmax_suppression;
    const std::string& descriptors_type = point_config.descriptors_type;
    out << YAML::Key << "point_detector_threshold_minimum" << YAML::Value << detector_threshold_minimum;
    out << YAML::Key << "point_detector_threshold_maximum_change" << YAML::Value << detector_threshold_maximum_change;
    out << YAML::Key << "point_detector_threshold_maximum" << YAML::Value << detector_threshold_maximum;
    out << YAML::Key << "point_detector_vertical_detectors" << YAML::Value << vertical_detectors;
    out << YAML::Key << "point_detector_horizontal_detectors" << YAML::Value << horizontal_detectors;
    out << YAML::Key << "point_detector_bin_size_pixels" << YAML::Value << bin_size_pixels;
    out << YAML::Key << "point_detector_target_number_of_keypoints_tolerance" << YAML::Value << target_number_of_keypoints_tolerance;
    out << YAML::Key << "point_detector_nonmax_suppression" << YAML::Value << nonmax_suppression;
    out << YAML::Key << "point_detector_descriptors_type" << YAML::Value << descriptors_type;
    //bdc  line detector
    const LineDetector::Config& line_config = _configuration.detector->lineDetector().config();
    const int& scale                 = line_config.scale;
    const int& num_octaves           = line_config.num_octaves;
    const float& lp2t_threshold      = line_config.lp2t_threshold;
    const float& bp2d_threshold      = line_config.bp2d_threshold;
    const int& width_of_band         = line_config.width_of_band;
    const float& distance_resolution = line_config.distance_resolution;
    const float& angle_resolution    = line_config.angle_resolution;
    const int& vote_threshold        = line_config.vote_threshold;
    const double& min_line_length    = line_config.min_line_length;
    const double& max_line_gap       = line_config.max_line_gap;
    out << YAML::Key << "line_detector_scale" << YAML::Value << scale;
    out << YAML::Key << "line_detector_num_octaves" << YAML::Value << num_octaves;
    out << YAML::Key << "line_detector_lp2t_threshold" << YAML::Value << lp2t_threshold;
    out << YAML::Key << "line_detector_bp2d_threshold" << YAML::Value << bp2d_threshold;
    out << YAML::Key << "line_detector_width_of_band" << YAML::Value << width_of_band;
    out << YAML::Key << "line_detector_distance_resolution" << YAML::Value << distance_resolution;
    out << YAML::Key << "line_detector_angle_resolution" << YAML::Value << angle_resolution;
    out << YAML::Key << "line_detector_vote_threshold" << YAML::Value << vote_threshold;
    out << YAML::Key << "line_detector_min_line_length" << YAML::Value << min_line_length;
    out << YAML::Key << "line_detector_max_line_gap" << YAML::Value << max_line_gap;
    //bdc  plane detector
    const PlaneDetector::Config& plane_config = _configuration.detector->planeDetector().config();
    const int& min_cluster_points = plane_config.min_cluster_points;
    out << YAML::Key << "plane_detector_min_cluster_points" << YAML::Value << min_cluster_points;    
    out << YAML::EndMap; // detector
  }

  void YamlParser::_writeTrackerConfig(YAML::Emitter& out) {

    //ia correspondance finder
    const NNCorrespondenceFinder::Config& nn_correspondence_finder_config = _configuration.tracker->aligner()->nnCorrespondenceFinder()->config();
    out << YAML::Key << "nn_correspondence_finder";
    out << YAML::BeginMap;
    out << YAML::Key << "direction_scale" << YAML::Value << nn_correspondence_finder_config.direction_scale;
    out << YAML::Key << "normal_scale" << YAML::Value << nn_correspondence_finder_config.normal_scale;
    out << YAML::Key << "leaf_range" << YAML::Value << nn_correspondence_finder_config.leaf_range;
    out << YAML::Key << "tree_max_distance" << YAML::Value << nn_correspondence_finder_config.tree_max_distance;
    out << YAML::Key << "hamming_point_threshold" << YAML::Value << nn_correspondence_finder_config.hamming_point_threshold;
    out << YAML::Key << "hamming_line_threshold" << YAML::Value << nn_correspondence_finder_config.hamming_line_threshold;
    out << YAML::EndMap;

    //ia aligner
    const ShapesAligner::Config& aligner_config = _configuration.tracker->aligner()->config();
    out << YAML::Key << "aligner";
    out << YAML::BeginMap;
    out << YAML::Key << "max_iterations" << YAML::Value << aligner_config.max_iterations;
    out << YAML::Key << "use_support_weight" << YAML::Value << aligner_config.use_support_weight;
    out << YAML::Key << "use_direct_solver" << YAML::Value << aligner_config.use_direct_solver;
    out << YAML::EndMap;

    //ia tracker
    const ShapeTracker::Config& tracker_config = _configuration.tracker->config();
    out << YAML::Key << "tracker";
    out << YAML::BeginMap;
    out << YAML::Key << "point_distance_threshold" << YAML::Value << tracker_config.point_distance_threshold;
    out << YAML::Key << "direction_distance_threshold" << YAML::Value << tracker_config.direction_distance_threshold;
    out << YAML::Key << "scene_percentage_for_break" << YAML::Value << tracker_config.scene_percentage_for_break;
    out << YAML::Key << "resolution" << YAML::Value << tracker_config.resolution;
    out << YAML::Key << "age_threshold_point" << YAML::Value << tracker_config.age_threshold_point;
    out << YAML::Key << "age_threshold_line" << YAML::Value << tracker_config.age_threshold_line;
    out << YAML::Key << "age_threshold_plane" << YAML::Value << tracker_config.age_threshold_plane;
    out << YAML::Key << "maximum_matching_distance" << YAML::Value << tracker_config.maximum_matching_distance;
    out << YAML::Key << "recursive_edge_generation" << YAML::Value << tracker_config.recursive_edge_generation;
    out << YAML::EndMap;

  }

  void YamlParser::_writeRelocalizerConfig(YAML::Emitter& out) {
    const Relocalizer::Config& relocalizer_config = _configuration.relocalizer->config();
    const size_t& maximum_matching_distance  = relocalizer_config.maximum_matching_distance;
    const size_t& frame_interspace           = relocalizer_config.frame_interspace;
    const size_t& min_relocalization_matches = relocalizer_config.min_relocalization_matches;
    const real&   min_relocalization_score   = relocalizer_config.min_relocalization_score;
    out << YAML::Key << "relocalizer";
    out << YAML::BeginMap;
    out << YAML::Key << "maximum_matching_distance" << YAML::Value << maximum_matching_distance;
    out << YAML::Key << "frame_interspace" << YAML::Value << frame_interspace;
    out << YAML::Key << "min_relocalization_matches" << YAML::Value << min_relocalization_matches;
    out << YAML::Key << "min_relocalization_score" << YAML::Value << min_relocalization_score;
    out << YAML::EndMap; // detector    
  }
  
  void YamlParser::_writeLoopCloserConfig(YAML::Emitter& out) {
    const LoopCloser::Config& lc_config = _configuration.loop_closer->config();
    out << YAML::Key << "loop_closer";
    out << YAML::BeginMap;
    out << YAML::Key << "num_iterations" << YAML::Value << lc_config.num_iterations;
    out << YAML::Key << "g2o_filename" << YAML::Value << lc_config.output_graph_filename;
    out << YAML::Key << "optimization_kernel" << YAML::Value << lc_config.optimization_kernel;
    out << YAML::Key << "use_kernel" << YAML::Value << lc_config.use_robust_kernel;
    out << YAML::Key << "online_optimization" << YAML::Value << lc_config.online_optimization;
    out << YAML::EndMap;
  }

  void YamlParser::_writeTxtIOParserConfig(YAML::Emitter& out) {
    const TxtioParser::Config& txtio_parser_config = _configuration.txtio_parser->config();
    out << YAML::Key << "message_topics";
    out << YAML::BeginMap;
    out << YAML::Key << "topic_depth" << YAML::Value << txtio_parser_config.topic_depth;
    out << YAML::Key << "topic_rgb" << YAML::Value << txtio_parser_config.topic_rgb;
    out << YAML::EndMap;
  }

}
