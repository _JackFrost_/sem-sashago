#include <sys/stat.h>
#include <system/sashago_system.h>
#include <unistd.h>
#include <string>
#include <fstream>

namespace srrg_sashago {

#define FPS_TIME_STEP 5.0
 
  SashagoSystem::SashagoSystem() {
    _detector     = new ShapeDetector();
    _tracker      = new ShapeTracker();
    _relocalizer  = new Relocalizer();
    _loop_closer  = new LoopCloser();
    _txtio_parser = new TxtioParser();
    _yaml_parser  = new YamlParser();
    
    _map    = new WorldMap();
    _scenes = new IntSceneMap();
    _rgb_timestamp = 0.0;

  }

  SashagoSystem::~SashagoSystem() {
    //ia clean id generators
    Scene::resetIdGenerator();
    ShapeTracker::resetVertexIdGenerator();

    //ia checkout things
    delete _detector;
    delete _tracker;
    delete _relocalizer;
    delete _loop_closer;
    delete _txtio_parser;
    delete _yaml_parser;
    
    delete _map;

	//_last_scene = 0;	// Don't delete this, it will be deleted from the IntSceneMap

    IntSceneMap::iterator it = _scenes->begin();
    IntSceneMap::iterator it_end = _scenes->end();
    while (it != it_end) {
      delete it->second;
      ++it;
    }

    _scenes->clear();

    delete _scenes;


    if (_map_viewer) {
      delete _map_viewer;
    }

    if (_image_viewer) {
      delete _image_viewer;
    }
  }

  void SashagoSystem::loadConfigurationFromFile(const std::string& yaml_file_) {
    //bdc setup yaml parser with file to read config
    _yaml_parser->mutableConfig().input_yaml_filename = yaml_file_;
    //bdc and pointers to object to configurate
    _yaml_parser->mutableConfig().system       = this;
    _yaml_parser->mutableConfig().detector     = _detector;
    _yaml_parser->mutableConfig().tracker      = _tracker;
    _yaml_parser->mutableConfig().relocalizer  = _relocalizer;
    _yaml_parser->mutableConfig().loop_closer  = _loop_closer;
    _yaml_parser->mutableConfig().txtio_parser = _txtio_parser;
    //bdc read file and configurate objects
    _yaml_parser->read();
  }

  void SashagoSystem::writeConfigurationToFile(const std::string& yaml_file_) {
    //ia get the config name
    std::string config_name;
    std::string source_filepath = yaml_file_.substr(0,yaml_file_.find_last_of("."));

    if ((int)source_filepath.find_last_of("/") > 0) {
      config_name = source_filepath.substr(yaml_file_.find_last_of("/")+1);
    } else {
      config_name = source_filepath;
    }
    if (_config.verbosity == VerbosityLevel::Debug)
      std::cerr << "[BagashaSystem::writeConfigurationToFile]| saving configuration with name "
                << FG_GREEN(config_name) << " [full path: " << yaml_file_ << "]" << std::endl;

    //bdc setup yaml parser with file to read config
    _yaml_parser->mutableConfig().input_yaml_filename = "";
    _yaml_parser->mutableConfig().output_yaml_filename = yaml_file_;
    _yaml_parser->mutableConfig().configuration_name = config_name;
    //bdc and pointers to object to configurate
    _yaml_parser->mutableConfig().system       = this;
    _yaml_parser->mutableConfig().detector     = _detector;
    _yaml_parser->mutableConfig().tracker      = _tracker;
    _yaml_parser->mutableConfig().relocalizer  = _relocalizer;
    _yaml_parser->mutableConfig().loop_closer  = _loop_closer;
    _yaml_parser->mutableConfig().txtio_parser = _txtio_parser;
    //bdc read file and configurate objects
    _yaml_parser->write();
  }

  void SashagoSystem::processDataFromFile(const std::string& filename_) {
    _txtio_parser->mutableConfig().dataset_filename = filename_;
    _txtio_parser->init();

    TxtioParser::ParserStatus status;

    TimeStats mean_time_stats;
    double cumulative_time = 0.0;
    size_t cumulative_message_number = 0;
    double fps = 0.0;
    double mean_fps = 0.0;
    size_t num_fps_sample = 0;

    while ((status = _txtio_parser->processData()) != TxtioParser::ParserStatus::Stop) {
      if (status == TxtioParser::ParserStatus::Unsync)
        continue;

      //ia check if someone wants to finish early
      if (_config.use_gui) {
        assert(_map_viewer && "[BagashaSystem::processDataFromFile]| unexpected error, quit");
        if (_map_viewer->terminationRequested())
          break;
      }

      //ia check if it's the first message
      if (!_detector->isCameraMatrixSet()) {
        _detector->setK(_txtio_parser->cameraMatrix());
        _detector->setRowsAndCols(_txtio_parser->imageRGB().rows, _txtio_parser->imageRGB().cols);
        _detector->init();
      }

      //ia do the magic
      double t0 = srrg_core::getTime();
      if (_image_viewer) {_image_viewer->lock();}
      setImages(_txtio_parser->imageRGB(), _txtio_parser->imageDepth(), _txtio_parser->timestamp(), _txtio_parser->gt());
      if (_image_viewer) {_image_viewer->unlock();}
      compute();

      cumulative_time += (srrg_core::getTime() - t0);
      ++cumulative_message_number;

      if (cumulative_time > FPS_TIME_STEP) {
        fps = cumulative_message_number/cumulative_time;
        mean_fps += fps;
        ++num_fps_sample;
        cumulative_time = 0.0f;
        cumulative_message_number = 0;
        if (_config.verbosity > VerbosityLevel::None) {
          std::printf("[BagashaSystem::processDataFromFile] fps counter = %6.2f\n", fps);
        }
      }


      if (_config.use_gui) {

        //ia check if someone paused the playback
        while (_map_viewer->pausePlayback()) {
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        while (_map_viewer->stepMode() && !_map_viewer->numSteps()) {
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        
      }

      if (_map_viewer) {_map_viewer->decrementStep();}

      if (_config.verbosity > VerbosityLevel::None)
        std::cerr << FG_YELLOW(DOUBLE_BAR) << std::endl << std::endl;

    }

    if (_config.verbosity > VerbosityLevel::None) {
      std::cerr << "[BagashaSystem::processDataFromFile]| mean processing fps = "
                << FG_BGREEN((double)((double)mean_fps/(double)num_fps_sample)) << FG_BGREEN(" Hz")
                << std::endl << _cumulative_time_stats/(real)_txtio_parser->messageNumber()
                << std::endl << FG_YELLOW(DOUBLE_BAR) << std::endl << FG_YELLOW(DOUBLE_BAR) << std::endl;
    }

    //ia saving things before optimization
    if (_config.save_open_loop_traj) {
      if (_config.save_graph)
        saveGraph((_loop_closer->config().output_graph_filename + "_pre_optimization"));
      if (_config.save_track_tum)
        saveOutputTrackStandard(true); //ia enable overwrite
      if (_config.save_track_kitti)
        saveOutputTrackKitti(true);    //ia enable overwrite
    }

    //ia optimize graph
    std::cerr << "[BagashaSystem::processDataFromFile]| optimizing final graph...";
    optimizeGraph();
    std::cerr << "done!\n\n";

    if (_config.save_final_traj) {
      if (_config.save_graph)
        saveGraph((_loop_closer->config().output_graph_filename + "_post_optimization"));
      if (_config.save_track_tum)
        saveOutputTrackStandard(false); //ia disable overwrite
      if (_config.save_track_kitti)
        saveOutputTrackKitti(false);    //ia disable overwrite
    }
  }


  std::shared_ptr<std::thread> SashagoSystem::processDataFromFileInThread(const std::string& filename_) {
    return std::make_shared<std::thread>([=] {processDataFromFile(filename_);});
  }


  void SashagoSystem::setupGUI(QApplication* qapp_) {
    if (! _config.use_gui)
      throw std::runtime_error("[BagashaSystem::initGUI]| configuration has no gui selected, please check your config file");

    if (!qapp_)
      throw std::runtime_error("[BagashaSystem::initGUI]| invalid QApplication, exit");

    _qapp_ptr = qapp_;
    _map_viewer = new MapViewer();
    _image_viewer = new ImageViewer();
  }


  void SashagoSystem::init() {
    WorldMap::resetLandmarkIDGenerator();

    //ia propagate verbosity if not already done
    _setVerbosityToAllModule();

    if (_scenes->size()) {
      IntSceneMap::iterator it = _scenes->begin();
      IntSceneMap::iterator it_end = _scenes->end();
      while (it != it_end) {
        delete it->second;
        ++it;
      }

      _scenes->clear();
    }

    //ia just in case
    Scene::resetIdGenerator();
    ShapeTracker::resetVertexIdGenerator();

    _tracker->setMap(_map);
    _tracker->setOptimizer(_loop_closer->optimizer());

    _relocalizer->setSceneContainer(_scenes);
    _loop_closer->setWorldMap(_map);
    _loop_closer->setSceneContainer(_scenes);

    //ia if we run with gui
    if (_config.use_gui) {
      _map_viewer->setQServer(_qapp_ptr);
      _map_viewer->setMachableMap(_map);
      _map_viewer->setSceneContainer(_scenes);
      _map_viewer->showViewport();
    }

    _stats.setZero();
    _cumulative_time_stats.setZero();


    if (_config.verbosity > VerbosityLevel::None)
      std::cerr << "[BagashaSystem::init]| system successfully initialized" << std::endl;
  }

  void SashagoSystem::setImages(const srrg_core::RGBImage& rgb_image_,
                                const srrg_core::RawDepthImage& depth_image_,
                                const double& rgb_msg_timestamp_,
                                const Isometry3& gt_pose_) {
    _current_rgb = rgb_image_;
    _current_depth = depth_image_;
    _rgb_timestamp = rgb_msg_timestamp_;
    _current_gt_pose = gt_pose_;
  }

  void SashagoSystem::compute() {


    double t0 = 0.0;
    Scene* fresh_scene = new Scene();
    fresh_scene->setTimestamp(_rgb_timestamp);
    fresh_scene->setGTPose(_current_gt_pose);
    _scenes->insert(std::make_pair(fresh_scene->sceneID(), fresh_scene));

    if (_map_viewer) {_map_viewer->lock();}
    _detector->setImages(_current_depth, _current_rgb);
    if (_map_viewer) {_map_viewer->unlock();}

    if (_config.use_gui) {
      _image_viewer->setCurrentImage(_current_rgb, _current_depth);
      _image_viewer->setCurrentScene(fresh_scene);
      _image_viewer->setCameraMatrix(_detector->K());
    }

    t0 = srrg_core::getTime();
    if (_image_viewer){_image_viewer->lock();}
    _detector->compute(fresh_scene);
    _stats.detection = srrg_core::getTime() - t0;


	// Intraframe semantics
	//if (_last_scene) {	// Only do this if we aren't in the first scene
	//	fresh_scene->integrateSemantics(_last_scene);
	//}
	//std::cout << "DETECT: " << fresh_scene->entries().size() << std::endl;


    t0 = srrg_core::getTime();
    _tracker->setMovingScene(fresh_scene);
    if (_map_viewer) {_map_viewer->lock();}
    if (_config.use_guess)
      _tracker->compute(_txtio_parser->cameraGuess().cast<real>());
    else
      _tracker->compute();
    if (_map_viewer) {_map_viewer->unlock();}
    if (_image_viewer){_image_viewer->unlock();}
    _stats.tracking = srrg_core::getTime() - t0;

    t0 = srrg_core::getTime();
    //ia relocalizer
    _relocalizer->setScene(fresh_scene);
    _relocalizer->compute();

    _loop_closer->setCurrentScene(fresh_scene);
    _loop_closer->setMatchingScene(_relocalizer->matchingScene());
    _loop_closer->setClosuresContraints(_relocalizer->constraints());

    if(_relocalizer->constraints().size()) {
      if (_map_viewer) {_map_viewer->lock();}
	  std::cerr << "LOOP CLOSED!" << std::endl;
      _loop_closer->compute();
      if (_map_viewer) {_map_viewer->unlock();}
    }
    _stats.relocalization = srrg_core::getTime() - t0;
    _cumulative_time_stats += _stats;

    if (_config.verbosity >= VerbosityLevel::Time) {
      std::cerr << _stats << std::endl;
      if (_config.verbosity >= VerbosityLevel::Debug) {
        std::cerr << "[BagashaSystem::compute]| num pool entries = " << _map->entries().size()
                  << "\tnum landmarks = " << _map->landmarks().size() << std::endl;
      }
    }

	// Update last scene
	//_last_scene = fresh_scene;
  }

  void SashagoSystem::saveGraph(const std::string& filename_) {
    _loop_closer->optimizer()->save(filename_.c_str());
  }

  void SashagoSystem::optimizeGraph() {
    _loop_closer->performBatchOptimization();
  }

  void SashagoSystem::saveOutputTrackStandard(const bool& overwrite_) {
    if (_config.filename_output_track_standard == "") {
      throw std::runtime_error("[BagashaSystem::saveOutputTrackStandard]| invalid filename, please check your configuration file");
    }

    //ia check if file exists
    std::string filename = _config.filename_output_track_standard;
    struct stat buffer;
    if (stat(filename.c_str(), &buffer) == 0 && !overwrite_) {
      std::string source_filepath = filename.substr(0,filename.find_last_of("."));
      source_filepath = source_filepath + "_2." + filename.substr(filename.find_last_of(".") + 1);
      filename = source_filepath;
      std::cerr << "[BagashaSystem::saveOutputTrackStandard] file already exists and overwrite flag is disabled, saving in " << FG_YELLOW(filename) << std::endl;
    } else {
      std::cerr << "[BagashaSystem::saveOutputTrackStandard] saving in " << FG_YELLOW(filename) << std::endl;
    }

    std::ofstream output_stream(filename);
    for (SceneContainer::iterator s_it = _scenes->begin(); s_it != _scenes->end();++s_it) {
      Scene* scene = s_it->second;
      output_stream << std::setprecision(std::numeric_limits<double>::digits10)
                    << scene->timeStamp() << " ";
      output_stream << std::setprecision(std::numeric_limits<float>::digits10)
                    << srrg_core::t2w(scene->pose()).transpose() << std::endl;
    }
    output_stream.close();
  }

  void SashagoSystem::saveOutputTrackKitti(const bool& overwrite_) {
    if (_config.filename_output_track_kitti == "") {
      throw std::runtime_error("[BagashaSystem::saveOutputTrackKitti]| invalid filename, please check your configuration file");
    }

    //ia check if file exists
    std::string filename = _config.filename_output_track_kitti;
    struct stat buffer;
    if (stat(filename.c_str(), &buffer) == 0 && !overwrite_) {
      std::string source_filepath = filename.substr(0,filename.find_last_of("."));
      source_filepath = source_filepath + "_2." + filename.substr(filename.find_last_of(".") + 1);
      filename = source_filepath;
      std::cerr << "[BagashaSystem::saveOutputTrackKitti] file already exists and overwrite flag is disabled, saving in " << FG_YELLOW(filename) << std::endl;
    } else {
      std::cerr << "[BagashaSystem::saveOutputTrackKitti] saving in " << FG_YELLOW(filename) << std::endl;
    }

    std::ofstream output_stream(filename);
    for (SceneContainer::iterator s_it = _scenes->begin(); s_it != _scenes->end();++s_it) {
      const Isometry3& T = s_it->second->pose();
      for (size_t r = 0; r < 3; ++r)
        for (size_t c = 0; c < 4; ++c)
          output_stream << std::setprecision(std::numeric_limits<double>::digits10) << T.matrix()(r,c) << " ";
      output_stream << std::endl;
    }

    output_stream.close();

  }

  void SashagoSystem::saveOutputTrackTxtio() {
    _txtio_parser->mutableConfig().output_filename = _config.filename_output_track_txtio;
    _txtio_parser->write(_scenes);
    //bdc print holy command to run the txtio w/ nicp tracker gui
    std::cerr << "[BagashaSystem::saveOutputTrackTxtio]: saved track as txtio file. You can visualize it by typing:\n";
    std::cerr << "\t rosrun srrg_nicp_tracker_gui srrg_nicp_tracker_gui_app -t " << _txtio_parser->config().topic_depth
              << " -rgbt " << _txtio_parser->config().topic_rgb
              << " -bpr 1 "
              << " -config do_nothing "
              << " -max_distance " << _detector->config().max_distance
              << " " << _txtio_parser->config().output_filename << std::endl;
  }


  void SashagoSystem::updateGUI() {
    if (_config.use_gui) {
      _map_viewer->update();
      _image_viewer->draw();

      //ia check if someone paused the playback
      while (_map_viewer->pausePlayback()) {
        _map_viewer->update();
        _image_viewer->draw();
      }

      while (_map_viewer->stepMode() && !_map_viewer->numSteps()) {
        _map_viewer->update();
        _image_viewer->draw();
      }
    }
  }

  
  
} //ia end namespace
