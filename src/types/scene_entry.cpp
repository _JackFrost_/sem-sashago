#include <Eigen/SVD>
#include <Eigen/Eigenvalues>
#include <iostream>
#include "srrg_image_utils/bresenham.h"

#include "scene_entry.h"
#include "scene.h"

namespace srrg_sashago {

  using namespace srrg_core;
  

  SceneEntry::SceneEntry(const Matchable& matchable_,
                         Scene* scene_ptr_) {
    _matchable = matchable_;
    _global_matchable = matchable_;

    //bookk
    _prev = 0;
    _next = 0;
    _scene_ptr = scene_ptr_;

    _descriptor = 0;
    _origin.setZero();
    _cloud.resize(0);
    _contour_cloud.resize(0);
    _is_good = true;
    _age = 0;

	_sema_idx = -1;
  }

  SceneEntry::~SceneEntry() {}

  void SceneEntry::connectSceneEntry(SceneEntry* entry_)  {
    SceneEntry* last_entry_in_track = entry_->_lastTrackEntry();
    if (last_entry_in_track->_scene_ptr != this->_scene_ptr) {
      _prev = last_entry_in_track;
      last_entry_in_track->_next = this;
      _age = last_entry_in_track->_age + 1;
      _landmark_ptr = last_entry_in_track->_landmark_ptr;
    } else {
      //ia jic
      _prev = 0;
      _next = 0;
      _age = 0;
    }
  }


  void SceneEntry::voxelizeCloud() {

    real x_min = std::numeric_limits<real>::max();
    real y_min = std::numeric_limits<real>::max();
    real z_min = std::numeric_limits<real>::max();
    Eigen::Vector3f centroid = Eigen::Vector3f::Zero();
    for(size_t i=0; i<_cloud.size(); ++i){
      const Eigen::Vector3f& point = _cloud[i].point();

      if(point.x() < x_min)
        x_min = point.x();
      if(point.y() < y_min)
        y_min = point.y();
      if(point.z() < z_min)
        z_min = point.z();

      centroid += point;
    }

    centroid /= (real)_cloud.size();
    Eigen::Vector3f origin(x_min,y_min,z_min);

    const real resolution = 0.01f;
    const real inverse_resolution = 1.0f/resolution;
    Eigen::Vector3f offset (resolution/2.0f,resolution/2.0f,0.0f);

    std::vector<Eigen::Vector3i, Eigen::aligned_allocator<Eigen::Vector3i> > ipoints(_cloud.size());
    const Eigen::Vector3f centroid_plus_origin = centroid + origin;
    for(size_t i=0; i<_cloud.size(); ++i){
      const Eigen::Vector3f& point = _cloud[i].point();
      Eigen::Vector3i idx = ((point-centroid_plus_origin)*inverse_resolution).cast<int>();
      ipoints[i] = idx;
    }

    std::sort(ipoints.begin(),
              ipoints.end(),
              [] (const Eigen::Vector3i& p1, const Eigen::Vector3i& p2) -> bool{
      for (int i=0; i<3; ++i) {
        if (p1[i]<p2[i])
          return true;
        if (p1[i]>p2[i])
          return false;
      }
      return false;
    });

    Cloud3D* grid_cloud = new Cloud3D();
    grid_cloud->resize(ipoints.size());
    int idx = -1;
    for (size_t i=1; i<ipoints.size(); ++i){
      if(idx>=0 && ipoints[i] == ipoints[i-1]){
        continue;
      } else {
        idx++;
        grid_cloud->at(idx) = RichPoint3D((origin+centroid) + ipoints[i].cast<real>()*resolution +offset,Eigen::Vector3f(0,0,0),1.0f);
      }
    }
    // TODO questa funzione fa schifo al cazzo
    grid_cloud->resize(idx+1);
    const int num_bins=60;
    real angle_res=num_bins/(2*M_PI);
    real angle_ires=1.f/angle_res;
    std::vector<real> ranges(num_bins);
    std::vector<int> indices(num_bins);
    std::fill(ranges.begin(), ranges.end(), -1.0f);
    std::fill(indices.begin(), indices.end(), -1);

    //simulate a 360 deg scan
    for(size_t i=0; i < grid_cloud->size(); ++i){
      const Eigen::Vector3f& point = grid_cloud->at(i).point();

      const real range = sqrt(point.x()*point.x() + point.y()*point.y());
      const real angle = atan2(point.x(),point.y())+M_PI;
      int bin=std::round(angle_res*angle);

      if (bin<0)
        bin=0;
      if (bin>=num_bins)
        bin=num_bins-1;
      if (ranges[bin]<range) {
        ranges[bin]=range;
        indices[bin]=i;
      }
    }

    delete grid_cloud;

    //fill vertex cloud
    _contour_cloud.clear();
    _contour_cloud.resize(num_bins);
    int k=0;

    for(int i=0; i < num_bins; ++i){

      if(indices[i] == -1) {
        continue;
      }
      real range = ranges[i];
      if(range < 0.05){
        continue;
      }

      real angle = (angle_ires*i)-M_PI;

      Eigen::Vector3f vertex(range*sin(angle), range*cos(angle), 0);
      _contour_cloud[k++] = RichPoint3D(vertex,Eigen::Vector3f(0,0,1),1.0f);
    }
    _contour_cloud.resize(k);

  }

  void SceneEntry::mergePlaneClouds(SceneEntry* entry_) {
    if (_matchable.type() != Matchable::Type::Plane)
      return;

    _cloud.add(entry_->cloud());
    voxelizeCloud();
  }

  Matrix3 SceneEntry::_getInformation() const {
    Matrix3 omega = Matrix3::Zero();
    real mean = 0;

    const size_t& cloud_size = _cloud.size();
    if(!cloud_size)
      return omega;
    
    for(const RichPoint3D& rp : _cloud) {
      mean += rp.point().norm();
    }

    mean /= (real) cloud_size;
    if(mean > 1e-4f)
      mean = 1.f / mean;
    else
      mean = 1.f;

    const real max = 5.f;
    const real min = 5e-2f;
    const real idiff = 1.f / (mean - min);
    const real pw = (max - min) * idiff;

    omega << pw, 0, 0,
              0, pw, 0,
              0, 0, pw;

    return omega;
  }
  
  Matrix3 SceneEntry::computeWeight() const {
    Matrix3 w = Matrix3::Zero();
    const real max = 5.f;
    const real min = 5e-2f;
    const real d = _matchable.point.norm();
    const real idiff = 1.f / (d - min);
    const real pw = (max - min) * idiff;

    switch(_matchable.type()) {
      case MatchableBase::Type::Point:
        w(0,0) = pw;
        w(1,1) = pw;
        w(2,2) = pw;
        break;
      case MatchableBase::Type::Line:
        w = _getInformation();
        break;
      case MatchableBase::Type::Plane:
        w = _getInformation();
        break;
      default:
        w = _getInformation();
        break;
    }

    return w;
  }

  void SceneEntry::_computeGlobalMatchable() {
    _global_matchable.transformInPlace(_scene_ptr->pose());
  }

  SceneEntry* SceneEntry::_lastTrackEntry() {
    SceneEntry* last = this;
    while (last->_next) {
      last = last->_next;
    }
    return last;

  }


}// end namespace


