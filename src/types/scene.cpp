#include "scene.h"

namespace srrg_sashago {

  uint64_t Scene::_id_generator = 0;

  Scene::Scene() {
    _pose = Eigen::Isometry3f::Identity();
    _gt_pose = Eigen::Isometry3f::Identity();
    _timestamp = -1;
    _id = _id_generator++;
    _is_pose_set = false;
  }

  Scene::~Scene() {
    clear();
  }

  void Scene::clear(const bool& destroy_) {
    _pose = Eigen::Isometry3f::Identity();
    _gt_pose = Eigen::Isometry3f::Identity();
    _timestamp = -1;
    _id = 0;

    if (destroy_) {
      for (size_t i = 0; i < _scene_entries.size(); ++i) {
        delete _scene_entries[i];
      }
    }
    _scene_entries.clear();
    _is_pose_set = false;
  }

  void Scene::setPose(const Isometry3& pose_) {
    _pose = pose_;
    for (size_t i = 0; i < _scene_entries.size(); ++i) {
      _scene_entries[i]->_computeGlobalMatchable();
    }
    _is_pose_set = true;
  }


  SceneEntry* Scene::addEntry(const Matchable& matchable_) {
    SceneEntry* new_entry = new SceneEntry(matchable_, this);
    _scene_entries.push_back(new_entry);
    return new_entry;
  }


  bool Scene::hasValidSemantics()
  {
	  return _sds.timestamp >= 0;
  }


  void Scene::integrateSemantics(Scene* other, float dist_thresh)
  {
	  // TODO
	  // Use a logic similar to this:
	  // 	fresh.integrateSemantics(last)
	  // 	<do NN_matching>
	  // 	for (SE s : fresh.getSEs()) {
	  // 		s_prev = s.findPenultimateTrack()
	  // 		if (s_prev.scene_ptr != last) continue
	  // 		sema_fresh = fresh.querySema(s)	// or maybe just the _sds index is sufficient to check?
	  //		sema_last = s_prev.scene_ptr->querySema(s_prev)	// same
	  //		if (_sema_links[sema_fresh] != sema_last)
	  //			penalize
	  //		else
	  //			pass
	  //		fi
	  // 	}

	  if (! other->hasValidSemantics()
			  || ! hasValidSemantics()) {
	  	return;
	  }

	  // Get semantic info from the other scene
	  SemaDetectionSet sds_in = other->getSemantics();
	  
	  // Nearest neighbor search:
	  // for each incoming entry, we seek the closest entry in the
	  // existing entries that has the same class. This will work
	  // fine for settings where elements of the same class are not
	  // very densely placed in the environment, otherwise it might
	  // be insufficient.
	  // NOTE: "distance" between entries is defined as the Euclidean
	  // norm between their top-left corners
	  // NOTE: A non-zero dist_thresh value will forbid any linking
	  // in case the distance is higher than the thresh, helps avoid
	  // occlusion-artifacts but may be bad for rapid camera movements,
	  // esp. when objects are close to the camera or if the framerate is
	  // low etc.
	  float dsq = dist_thresh * dist_thresh;
	  _sema_links.resize(_sds.scores.size());
	  std::fill(_sema_links.begin(), _sema_links.end(), -1);	// Initialize to -1
	  for (size_t i=0; i<sds_in.scores.size(); i++) {
		int minDist = -1;
	  	int classID_ = sds_in.classIDs[i];
		int tlx_ = (int)sds_in.bbs[i].x, tly_ = (int)sds_in.bbs[i].y;

		// Iterate through this scene's detection set
		for (size_t j=0; j<_sds.scores.size(); j++) {
			int cl = _sds.classIDs[j];
			if (cl != classID_) {	// If class is not the same, drop it
				continue;
			}

			int _tlx = (int)_sds.bbs[j].x, _tly = (int)_sds.bbs[j].y;
			int dist = (tlx_ - _tlx)*(tlx_ - _tlx)
				+ (tly_ - _tly)*(tly_ - _tly);
			if (dsq != 0.0 && dist > dsq) {	// If thresh is active & distance is higher, drop it
				continue;
			}
			
			// If distance is better or this is the first match,
			// then proceed to overwrite
			if (minDist == -1 || minDist > dist) {	
				minDist = dist;
				_sema_links[j] = i;	// Note the storage convention!
			}
		}
	  }
  }

  void Scene::assocSemantics()
  {
	if (! hasValidSemantics()) return;

	// NOTE: One BB can contain multiple entries, but an entry
	// shall only belong to the BB of least area!
	for (SceneEntry* sep : _scene_entries) {
		cv::Point rep = sep->getSemaRep();
	    int minArea = -1;
		float maxScore = 0.0;
		for (size_t i=0; i<_sds.classIDs.size(); i++) {
			// Extract the next batch of data from the _sds
			cv::Rect bb = _sds.bbs[i];
			float score = _sds.scores[i];
			int classID = _sds.classIDs[i];

			if (bb.contains(rep)	// Representative must be within BB
					&& (score + 0.05 >= maxScore	// New score must not be lower than 0.05 below current
						&& (minArea == -1 || minArea > bb.area()))) {	// Area must be smaller
				minArea = bb.area();	// Update min area, then overwrite the association
				maxScore = score;		// Update score as well
				
				// The SceneEntry will know what index of _sds it belongs to, aka which detection
				// This means that the SceneEntry can recover all related data by accessing the
				// _sds item of its Scene, then retrieving the i'th entry of the required field(s).
				sep->addSemaAssoc(i);
			}
		} 
	}
	
  }

} //ia end namespace
