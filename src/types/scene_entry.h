#pragma once 
#include <srrg_types/defs.h>
#include <srrg_types/cloud_3d.h>
#include <opencv2/core/eigen.hpp> 
#include <vector>
#include "matchable.h"

namespace srrg_sashago {

  class Scene;
  class Landmark;

  class SceneEntry {
  public:
    SceneEntry() = delete;

    //! @brief connects a guy TO THE END of the track
    void connectSceneEntry(SceneEntry* entry_);

    //! @brief inline get/set method
    inline const Matchable& matchable() const {return _matchable;}
    inline const Matchable& globalMatchable() const {return _global_matchable;}

    inline SceneEntry* prevSceneEntry() const {return _prev;}
    inline SceneEntry* nextSceneEntry() const {return _next;}
    inline Landmark* landmark() const {return _landmark_ptr;}
    inline Scene* scene() const {return _scene_ptr;}
    
    inline const cv::Mat& descriptor() const {return _descriptor;}
    inline const Vector2& extent() const {return _extent;}
    inline const Vector3& origin() const {return _origin;}
    inline const srrg_core::Cloud3D& cloud() const {return _cloud;}
    inline const srrg_core::Cloud3D& contourCloud() const {return _contour_cloud;}

    inline const size_t& age() const {return _age;}
    inline const bool& isGood() const {return _is_good;}

    inline void setLandmarkPtr(Landmark* ptr_) {_landmark_ptr = ptr_;}
    inline void setDescriptor(const cv::Mat& desc_) {_descriptor = desc_;}
    inline void setExtent(const Vector2& extent_) {_extent = extent_;}
    inline void setOrigin(const Vector3& origin_) {_origin = origin_;}
    inline void setCloud(const srrg_core::Cloud3D& cloud_) {_cloud = cloud_;}
    inline void setIsGood(const bool& flag_) {_is_good = flag_;}

    //! @brief additional misc stuff
    Matrix3 computeWeight() const;
    void voxelizeCloud();
    void mergePlaneClouds(SceneEntry* entry_);
	
	// Set/get semantic stuff
	void addSemaAssoc(int bbIdx) { _sema_idx = bbIdx; };
	int getSemaAssoc() { return _sema_idx; }
	void setSemaRep(const cv::Point _pt) { _semaRep = _pt; }
	cv::Point getSemaRep() { return _semaRep; }
	bool hasValidSema() { return _sema_idx >= 0; }

  protected:
    SceneEntry(const Matchable& matchable_, Scene* scene_ptr_);
    virtual ~SceneEntry();

    Matrix3 _getInformation() const;
    void _computeGlobalMatchable();
    SceneEntry* _lastTrackEntry();

    //! each puttana has 2 matchables, one in the current scene frame the other in the global puttana
    Matchable _matchable;
    Matchable _global_matchable;

    //! @brief bookkeping
    SceneEntry* _prev = 0;
    SceneEntry* _next = 0;
    Scene* _scene_ptr = 0;
    Landmark* _landmark_ptr = 0;

    //! @brief matchable descriptor (empty if Plane)
    cv::Mat _descriptor;

    //! @brief draw helpers
    Vector2    _extent;
    Vector3    _origin;
    srrg_core::Cloud3D _cloud;
    srrg_core::Cloud3D _contour_cloud;

    //! @brief misc
    size_t _age;
    bool _is_good;


	// A single point for semantic representation
	cv::Point _semaRep;
	int _sema_idx;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    friend class Scene;
    friend class Landmark;
    friend class WorldMap; //ia this is a shit
  };

  using SceneEntrySet = std::set<SceneEntry*, std::less<SceneEntry*>, Eigen::aligned_allocator<SceneEntry*> >;
  using SceneEntryVector = std::vector<SceneEntry*, Eigen::aligned_allocator<SceneEntry*> >;
} // end namespace srrg_matchable
