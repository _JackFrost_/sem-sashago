#pragma once
#include <g2o/core/sparse_optimizer.h>

#include <g2o/apps/g2o_cli/dl_wrapper.h>
#include <g2o/apps/g2o_cli/output_helper.h>
#include <g2o/apps/g2o_cli/g2o_common.h>

#include <g2o/config.h>
#include <g2o/core/block_solver.h>
#include <g2o/solvers/cholmod/linear_solver_cholmod.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/core/optimization_algorithm.h>
#include <g2o/core/optimization_algorithm_gauss_newton.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/core/factory.h>
#include <g2o/core/robust_kernel_impl.h>
#include <g2o/core/robust_kernel.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/sparse_optimizer_terminate_action.h>

#include "types/scene.h"
#include "types/world_map.h"
#include "types/constraint.h"
#include "aligner/direct_registration_solver.h"
#include "aligner/iterative_registration_solver.h"
#include "aligner/nn_correspondence_finder.h"

namespace srrg_sashago {

  class LoopCloser {
  public:

    using SlamBlockSolver = g2o::BlockSolver<g2o::BlockSolverTraits<-1, -1>>;
    using SlamLinearSolverCholmod = g2o::LinearSolverCholmod<SlamBlockSolver::PoseMatrixType>;
    using SlamLinearSolverCSparse = g2o::LinearSolverCSparse<SlamBlockSolver::PoseMatrixType>;

    struct Config {
      VerbosityLevel verbosity;
      size_t num_iterations;

      //! @brief g2o things
      std::string output_graph_filename;
      std::string optimization_kernel;
      real optimization_kernel_width;

      bool use_robust_kernel;
      bool online_optimization;

      Config() {
        verbosity = VerbosityLevel::Info;
        num_iterations = 10;

        output_graph_filename = "jesus.g2o";
        optimization_kernel = "Cauchy";

		// DAMPING
        optimization_kernel_width = 0.01; //0.005; //1.0;

        use_robust_kernel = false;
        online_optimization = false;
      }
    };

    //! @breif ctor/dtor
    LoopCloser();
    virtual ~LoopCloser();

    //! @brief inline set get
    inline const Config& config() const {return _config;}
    inline Config& mutableConfig() {return _config;}

    inline g2o::SparseOptimizer* optimizer() const {return _optimizer;}

    inline void setClosuresContraints(const ConstraintVector& constraints_) {_closure_constraints = constraints_;}
    inline void setCurrentScene(Scene* scene_) {_current_scene = scene_;}
    inline void setMatchingScene(Scene* scene_) {_matching_scene = scene_;}
    inline void setSceneContainer(IntSceneMap* scenes_) {_scenes = scenes_;}
    inline void setWorldMap(WorldMap* map_) {_map = map_;}

    //! @brief checks if there are loop closures detected from the relocalizer and
    //!        updates both the map and the graph accordingly
    void compute();

    //! @brief batch optimization of the g2o graph
    void performBatchOptimization();

  protected:
    //! @brief graph optimizer - owned by this module
    g2o::SparseOptimizer* _optimizer = 0;

    //! @brief g2o factory to generate robust kernels
    g2o::AbstractRobustKernelCreator*   _factory_kernel = 0;

    //! @brief ptrs to stuff that we will modify during the optimization process
    Scene* _current_scene  = 0;
    Scene* _matching_scene = 0;
    WorldMap* _map = 0;
    IntSceneMap* _scenes = 0;

    //! @brief constraints deriving from the relocalization
    ConstraintVector _closure_constraints;

    Config _config;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace srrg_bagasha */

