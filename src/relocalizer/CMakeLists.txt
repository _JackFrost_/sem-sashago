add_library(srrg_sashago_relocalizer_library SHARED
  relocalizer.h
  relocalizer.cpp)

target_link_libraries(srrg_sashago_relocalizer_library
  srrg_image_utils_library
  srrg_sashago_types_library
  srrg_sashago_detector_library
  srrg_sashago_aligner_library
  ${SRRG_G2O_LIBRARIES})
