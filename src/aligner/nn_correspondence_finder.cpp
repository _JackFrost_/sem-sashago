#include "nn_correspondence_finder.h"

namespace srrg_sashago {

  NNCorrespondenceFinder::NNCorrespondenceFinder() :
                  _fixed_size(0),
                  _moving_size(0)
  {
    //ia nothing to do here
  }

  NNCorrespondenceFinder::~NNCorrespondenceFinder() {
    if(_kd_tree_point)
      delete _kd_tree_point;
    if(_kd_tree_line)
      delete _kd_tree_line;
    if(_kd_tree_plane)
      delete _kd_tree_plane;
  }

  void NNCorrespondenceFinder::model2linear() {
    if(!_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder::model2linear]: null pointer");
    //    _reference_matchables.resize(fixed_size);

    _reference_matchable_points.clear();
    _reference_matchable_lines.clear();
    _reference_matchable_planes.clear();
    _reference_point_map.clear();
    _reference_line_map.clear();
    _reference_plane_map.clear();

    for(size_t i = 0; i < _fixed_size; ++i) {

      //      std::cerr << "_fixed->numEntries(): " << _fixed->numEntries() << std::endl;
      //      std::cerr << "_fixed->entries()[i]->matchable():" << _fixed->entries()[i]->matchable() << std::endl;
      //      std::cerr << "_fixed->entries()[i]->matchable():" << _fixed->entries()[i]->globalMatchable() << std::endl;

      const Matchable& fixed_m = _fixed->entries()[i]->globalMatchable();

      switch (fixed_m.type()) {
      case Matchable::Type::Point:
      {
        _reference_matchable_points.push_back(fixed_m.point);
        _reference_point_map.push_back(i);
        break;
      }
      case Matchable::Type::Line:
      {
        Vector6 line_stuff;
        line_stuff.head<3>() = (fixed_m.directionVector()).cross(fixed_m.point);
        line_stuff.tail<3>() = fixed_m.directionVector() * _config.direction_scale;
        _reference_matchable_lines.push_back(line_stuff);
        _reference_line_map.push_back(i);
        break;
      }
      case Matchable::Type::Plane:
      {
        Vector4 plane_stuff;
        plane_stuff.head<3>() = fixed_m.directionVector() * _config.normal_scale;
        plane_stuff(3) = fixed_m.directionVector().dot(fixed_m.point);
        _reference_matchable_planes.push_back(plane_stuff);
        _reference_plane_map.push_back(i);
        break;
      }
      default:
        throw std::runtime_error("[NNCorrespondenceFinder::model2linear] unknown matchable type");
      }
    }

  }

  void NNCorrespondenceFinder::init() {
    if(!_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder::init] scenes not initialized!");

    //bdc,  linearize the _fixed data in a KDVEctor
    model2linear();

    //bdc,  create the yellow lemon tree
    //ia lol
    if(_kd_tree_point)
      delete _kd_tree_point;
    _kd_tree_point = 0;
    _kd_tree_point = new srrg_core::KDTree<real, KDTREE_DIM_POINT>(_reference_matchable_points, _config.leaf_range);

    //bdc,  create the yellow lemon tree
    if(_kd_tree_line)
      delete _kd_tree_line;
    _kd_tree_line = 0;
    _kd_tree_line = new srrg_core::KDTree<real, KDTREE_DIM_LINE>(_reference_matchable_lines,  _config.leaf_range);

    //bdc,  create the yellow lemon tree
    if(_kd_tree_plane)
      delete _kd_tree_plane;
    _kd_tree_plane = 0;
    _kd_tree_plane = new srrg_core::KDTree<real, KDTREE_DIM_PLANE>(_reference_matchable_planes,  _config.leaf_range);

  }

  // it takes as T_, the transform of the moving w.r.t. the fixed, i.e. T_fm
  // in fact, it pre-multiplies this T_ to the moving scene
  void NNCorrespondenceFinder::compute(ConstraintVector& constraints_,
      const Isometry3& T_) {

    if(!_kd_tree_point || !_kd_tree_line || !_kd_tree_plane ||
        !_fixed || !_moving) {
      throw std::runtime_error("[NNCorrespondenceFinder::compute]: data structures not initialized");
    }

    _stats.setZero();

    //bdc,  resize constraints to max size
    constraints_.resize(_moving_size);
    size_t constraints_count = 0;

    for(size_t i = 0; i < _moving_size; ++i) {
      SceneEntry* moving_entry = _moving->entries()[i];
      SceneEntry* fixed_entry = 0;

      //bdc,  transform moving Matchable
      const Matchable moving_matchable_trans = moving_entry->matchable().transform(T_);

      switch (moving_matchable_trans.type()) {
      case MatchableBase::Type::Point:
      {
        fixed_entry = _queryPoint(moving_entry, moving_matchable_trans);
        break;
      }
      case MatchableBase::Type::Line:
      {
        fixed_entry = _queryLine(moving_entry, moving_matchable_trans);
        break;
      }
      case MatchableBase::Type::Plane:
      {
        fixed_entry = _queryPlane(moving_entry, moving_matchable_trans);
        break;
      }
      default:
        throw std::runtime_error("[NNCorrespondenceFinder::compute]| unknown matchable type");
      }


      //ia if data association wasn't good on this one, continue
      if (!fixed_entry)
        continue;

	  /**
#ifdef USE_SEMANTICS 
	  // OVERSIMPLIFIED: Hard ignore anything not matching the sema label!
	  int midx = moving_entry->getSemaAssoc(), fidx = fixed_entry-> getSemaAssoc();
	  if (midx == -1 || fidx == -1)
		continue;
	  int mcl = moving_entry->scene()->getSemantics().classIDs[midx];
	  int fcl = fixed_entry->scene()->getSemantics().classIDs[fidx];
	  if (mcl != fcl)
	  	continue;
#endif
		**/

      // then to the constraints' set
      constraints_[constraints_count] = Constraint(fixed_entry,moving_entry,false);
      constraints_count++;

    }

    constraints_.resize(constraints_count);
//
//    if(_config.verbosity == VerbosityLevel::Debug){
//      std::cerr << "[NNCorrespondenceFinder::compute]|find: " << constraints_count << " correspondences. Of which:";
//      std::cerr << _stats << std::endl;
//    }

  }


  SceneEntry* NNCorrespondenceFinder::_queryPoint(SceneEntry* query_,
                                                  const Matchable& transformed_matchable_) {
    srrg_core::KDTree<real, KDTREE_DIM_POINT>::VectorTD query_point = transformed_matchable_.point;
    srrg_core::KDTree<real, KDTREE_DIM_POINT>::VectorTD matched_point;
    SceneEntry* matched_entry = 0;

	/*** Efficiency ***/
	
	//ia if there are points, compute the distance and the tree index
    real tree_distance = 0.0;
    int tree_index = -1;
    if(_reference_matchable_points.size()) {
      tree_distance = _kd_tree_point->findNeighbor(matched_point, tree_index, query_point, _config.tree_max_distance);
	}
	
	real cutoff = 9e-4;	// 3cm squared

    //ia if there has been no match return nothing
    if (tree_index < 0) {
			//|| tree_distance > cutoff) {
      _stats.unassociated++;
      return nullptr;
    }
	/***/

	// --- DEBUGGING ---
	/*
    //ia if there are points, compute the distance and the tree index
    real tree_distance = 0.0;
    int orig_tree_index = -1;
    if(_reference_matchable_points.size()) {
      tree_distance = _kd_tree_point->findNeighbor(matched_point, orig_tree_index, query_point, _config.tree_max_distance);
	}

    //ia if there has been no match return nothing
    if (orig_tree_index < 0) {
      _stats.unassociated++;
      return nullptr;
    }
	***/

	/*** Accuracy ***
	srrg_core::KDTree<real, KDTREE_DIM_POINT>::VectorTDVector resp;
	std::vector<int> idxs;
	if (_reference_matchable_points.size()) {	
		_kd_tree_point->findNeighbors(resp, idxs, query_point, _config.tree_max_distance/10.0);
		//std::cout << "NBs found: " << resp.size() << ", " << idxs.size() << std::endl;
	}

	if (! idxs.size()) {
		_stats.unassociated++;
		//std::cout << "P O I N T ::: FAILURE!!!" << std::endl;
		return nullptr;
	}
	//bool nearestFound = std::count(idxs.begin(), idxs.end(), orig_tree_index);
	//std::cout << "P O I N T >>> SUCCESS " << resp.size() << " <<<" << (nearestFound ? " [yes]" : " [no]") << std::endl;
	
	real dd = std::numeric_limits<real>::max();
	int tree_index = -1;
	for (size_t i=0; i<resp.size(); i++) {
		matched_entry = _fixed->entries()[_reference_point_map.at(idxs[i])];
    	real dd_cur = cv::norm(matched_entry->descriptor(), query_->descriptor(), cv::NORM_HAMMING);
		real gd_cur = (matched_entry->globalMatchable().point - transformed_matchable_.point).norm();
		//std::cout << "Geom VS Desc: " << gd_cur << " :: " << dd_cur << std::endl;
		real geom_weight=1.0e3, desc_weight=1.0;
		if (desc_weight*dd_cur + geom_weight*gd_cur < dd) {
			dd = desc_weight*dd_cur + geom_weight*gd_cur;
			tree_index = idxs[i];
		}
	}
	***/

    //ia we got a match
    matched_entry = _fixed->entries()[_reference_point_map.at(tree_index)];

    //ia check if this match is good based on hamming distance
    real descriptor_distance = cv::norm(matched_entry->descriptor(), query_->descriptor(), cv::NORM_HAMMING);
    if(descriptor_distance > _config.hamming_point_threshold) {
      _stats.hamming_discard++;
      _stats.unassociated++;
      query_->setIsGood(false);
      return nullptr;
    }

    //ia this is good
    _stats.ass_points++;
    return matched_entry;
  }


  SceneEntry* NNCorrespondenceFinder::_queryLine(SceneEntry* query_,
                                                 const Matchable& transformed_matchable_) {
    srrg_core::KDTree<real, KDTREE_DIM_LINE>::VectorTD query_line;
    srrg_core::KDTree<real, KDTREE_DIM_LINE>::VectorTD matched_line;
    query_line.head<3>() = transformed_matchable_.directionVector().cross(transformed_matchable_.point);
    query_line.tail<3>() = transformed_matchable_.directionVector() * _config.direction_scale;
    SceneEntry* matched_entry = 0;

	/*** Efficiency ***/
    //ia if there are lines in the tree, compute geometric distance and the tree index
    real tree_distance = 0.0;
    int tree_index = -1;
    if(_reference_matchable_lines.size())
      tree_distance = _kd_tree_line->findNeighbor(matched_line, tree_index, query_line, _config.tree_max_distance);

	real cutoff = 9e-4;

    //ia if there has been no match return
    if(tree_index < 0) {
			//|| tree_distance > cutoff) {
      _stats.unassociated++;
      return nullptr;
    }
	/***/

	// --- DEBUGGING ---
	/*
    //ia if there are lines in the tree, compute geometric distance and the tree index
    real tree_distance = 0.0;
    int orig_tree_index = -1;
    if(_reference_matchable_lines.size())
      tree_distance = _kd_tree_line->findNeighbor(matched_line, orig_tree_index, query_line, _config.tree_max_distance);

    //ia if there has been no match return
    if(orig_tree_index < 0) {
      _stats.unassociated++;
      return nullptr;
    }
	*/
	
	/*** Accuracy ***
	srrg_core::KDTree<real, KDTREE_DIM_LINE>::VectorTDVector resp;
	std::vector<int> idxs;
	if (_reference_matchable_lines.size()) {
		_kd_tree_line->findNeighbors(resp, idxs, query_line, _config.tree_max_distance/10.0);
		//std::cout << "NBs found: " << resp.size() << ", " << idxs.size() << std::endl;
	}

	if (! idxs.size()) {
		_stats.unassociated++;
		//std::cout << "L I N E ::: FAILURE!!!" << std::endl;
		return nullptr;
	}
	
	//bool nearestFound = std::count(idxs.begin(), idxs.end(), orig_tree_index);
	//std::cout << "L I N E >>> SUCCESS " << resp.size() << " <<<" << (nearestFound ? " [yes]" : " [no]") << std::endl;

	real dd = std::numeric_limits<real>::max();
	int tree_index = -1;
	for (size_t i=0; i<resp.size(); i++) {
		matched_entry = _fixed->entries()[_reference_line_map.at(idxs[i])];
    	real dd_cur = cv::norm(matched_entry->descriptor(), query_->descriptor(), cv::NORM_HAMMING);
		real gd_cur = (matched_entry->globalMatchable().point - transformed_matchable_.point).norm();
		//std::cout << "Geom VS Desc: " << gd_cur << " :: " << dd_cur << std::endl;
		real geom_weight=1.0e3, desc_weight=1.0;
		if (desc_weight*dd_cur + geom_weight*gd_cur < dd) {
			dd = desc_weight*dd_cur + geom_weight*gd_cur;
			tree_index = idxs[i];
		}
	}
	***/

    //ia get the goddamn line from the pull
    matched_entry = _fixed->entries()[_reference_line_map.at(tree_index)];

    //ia check if the match is good
    real dist = cv::norm(matched_entry->descriptor(), query_->descriptor(), cv::NORM_HAMMING);
    if(dist > _config.hamming_line_threshold) {
      _stats.hamming_discard++;
      _stats.unassociated++;
      query_->setIsGood(false);
      return nullptr;
    }

    _stats.ass_lines++;
    return matched_entry;
  }


  SceneEntry* NNCorrespondenceFinder::_queryPlane(SceneEntry* query_,
                                                  const Matchable& transformed_matchable_) {
    srrg_core::KDTree<real, KDTREE_DIM_PLANE>::VectorTD query_plane;
    srrg_core::KDTree<real, KDTREE_DIM_PLANE>::VectorTD matched_plane;
    query_plane.head<3>() = transformed_matchable_.directionVector() * _config.normal_scale;
    query_plane(3) = transformed_matchable_.directionVector().dot(transformed_matchable_.point);
    SceneEntry* matched_entry = 0;

	/***/
    //ia if there are planes compute tree leaf and tree index
    real tree_distance = 0.0;
    int tree_index = -1;
    if(_reference_matchable_planes.size())
      tree_distance = _kd_tree_plane->findNeighbor(matched_plane, tree_index, query_plane, _config.tree_max_distance);

    if(tree_index < 0) {
      _stats.unassociated++;
      return nullptr;
    }
	/***/

	/*** Accuracy - UNTESTED ***
	srrg_core::KDTree<real, KDTREE_DIM_PLANE>::VectorTDVector resp;
	std::vector<int> idxs;
	if (_reference_matchable_planes.size()) {	
		_kd_tree_plane->findNeighbors(resp, idxs, query_plane, _config.tree_max_distance*10);
		//std::cout << "NBs found: " << resp.size() << ", " << idxs.size() << std::endl;
	}

	if (! idxs.size()) {
		_stats.unassociated++;
		return nullptr;
	}
	
	real dd = std::numeric_limits<real>::max();
	int tree_index = -1;
	for (size_t i=0; i<resp.size(); i++) {
		matched_entry = _fixed->entries()[_reference_plane_map.at(idxs[i])];
    	real dd_cur =
			- matched_entry->matchable().directionVector()
			.dot(query_->matchable().directionVector()) +
			std::fabs(
					matched_entry->matchable().directionVector()
					.dot(matched_entry->matchable().point) - 
					(query_->matchable().directionVector())
					.dot(query_->matchable().point));
		if (dd_cur < dd) {
			dd = dd_cur;
			tree_index = i;
		}
	}
	***/

    matched_entry = _fixed->entries()[_reference_plane_map.at(tree_index)];

    //ia check if plane is good
    real dist = matched_entry->matchable().directionVector().dot(query_->matchable().directionVector());
    real dist_d = std::fabs(matched_entry->matchable().directionVector().dot(matched_entry->matchable().point) -
                  (query_->matchable().directionVector()).dot(query_->matchable().point));
    if(dist < 0.8 || dist_d > 0.1) {
      _stats.unassociated++;
      query_->setIsGood(false);
      return nullptr;
    }

    _stats.ass_planes++;
    return matched_entry;
  }

} //ia end namespace srrg_bagasha
