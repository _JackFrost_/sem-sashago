#pragma once

#include <srrg_kdtree/kd_tree.hpp>
#include <srrg_system_utils/system_utils.h>

#include "types/world_map.h"
#include "aligner/base_registration_solver.h"

namespace srrg_sashago {

#define KDTREE_DIM_POINT  3
#define KDTREE_DIM_LINE   6
#define KDTREE_DIM_PLANE  4 

  class NNCorrespondenceFinder{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config{
      VerbosityLevel verbosity;

      real direction_scale;
      real normal_scale;
      real leaf_range;
      real tree_max_distance;
      size_t hamming_point_threshold;
      size_t hamming_line_threshold;

      Config() {
        verbosity = VerbosityLevel::Info;

        direction_scale = 1;
        normal_scale = 10;
        leaf_range = 0.9;
        tree_max_distance = 1.0;
        hamming_point_threshold = 25; //TODO check this value
        hamming_line_threshold = 25;
      }
    };

    struct Stats {
      size_t unassociated;
      size_t ass_lines;
      size_t ass_points;
      size_t ass_planes;
      size_t hamming_discard;

      Stats() {
        setZero();
      }

      inline void setZero() {
        unassociated = 0;
        ass_lines = 0;
        ass_points = 0;
        ass_planes = 0;
        hamming_discard = 0;
      }

      friend std::ostream& operator<<(std::ostream& os_, const Stats& stats_) {
        os_ << "point-point: " << stats_.ass_points
            << "  line-line:   " << stats_.ass_lines
            << "  plane-plane:   " << stats_.ass_planes
            << "  - un_ass:    " << stats_.unassociated
            << "  hamming_discarded: " << stats_.hamming_discard;
        return os_;
      }

      
    };

    //! @brief ctor - dtor
    NNCorrespondenceFinder();
    virtual ~NNCorrespondenceFinder();

    //! @brief inline set/get methods
    inline const Config& config() const {return _config;}
    inline Config& mutableConfig() {return _config;}

    inline void setScenes(WorldMap* fixed_,
        Scene* moving_) {
      _fixed = fixed_;
      _fixed_size = _fixed->numEntries();

      _moving = moving_;
      _moving_size = _moving->entries().size();
    }

    // initializes the tree from the fixed
    void init();

    // query the three with the moving
    void compute(ConstraintVector& constraints_,
        const Isometry3& T_ = Isometry3::Identity());

    inline const Stats& stats() const {return _stats;}


  protected:
    // write the fixed scene as a VectorTDVector. Called in the init,
    // during the tree construction
    void model2linear();

    //ia queries for the tree
    SceneEntry* _queryPoint(SceneEntry* query_,
                            const Matchable& tranformed_matchable_);
    SceneEntry* _queryLine(SceneEntry* query_,
                           const Matchable& tranformed_matchable_);
    SceneEntry* _queryPlane(SceneEntry* query_,
                            const Matchable& tranformed_matchable_);

    Config _config;

    srrg_core::KDTree<real, KDTREE_DIM_POINT>* _kd_tree_point = 0;
    srrg_core::KDTree<real, KDTREE_DIM_POINT>::VectorTDVector _reference_matchable_points;
    std::vector<size_t> _reference_point_map;

    srrg_core::KDTree<real, KDTREE_DIM_LINE>* _kd_tree_line = 0;
    srrg_core::KDTree<real, KDTREE_DIM_LINE>::VectorTDVector _reference_matchable_lines;
    std::vector<size_t> _reference_line_map;

    srrg_core::KDTree<real, KDTREE_DIM_PLANE>* _kd_tree_plane = 0;
    srrg_core::KDTree<real, KDTREE_DIM_PLANE>::VectorTDVector _reference_matchable_planes;
    std::vector<size_t> _reference_plane_map;

    WorldMap* _fixed = 0;
    Scene* _moving = 0;

    size_t _fixed_size;
    size_t _moving_size;
    Stats _stats;
  };



} //ia end namespace srrg_bagasha
