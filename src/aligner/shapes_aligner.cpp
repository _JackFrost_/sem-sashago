#include "shapes_aligner.h"

#define IGN_OL true

namespace srrg_sashago {


  ShapesAligner::ShapesAligner():
    _map_fixed(NULL),
    _scene_moving(NULL),
    _T(Isometry3::Identity()) {
    _ass = new NNCorrespondenceFinder();
  }
  
  ShapesAligner::~ShapesAligner() {
    delete _ass;
  }

  void ShapesAligner::_computeDirect(const Isometry3& init_guess_){
    double t0 = 0;
    _direct_solver.init(init_guess_, _configuration.use_support_weight);
    _direct_solver.setIgnoreOutliers(IGN_OL);
    t0 = srrg_core::getTime();
    _ass->compute(_direct_solver.constraints(), _direct_solver.transform());
    _stats.data_association += srrg_core::getTime() - t0;
    t0 = srrg_core::getTime();
    _direct_solver.oneRound();
    _stats.solve += srrg_core::getTime() - t0;

    if (_configuration.verbosity == VerbosityLevel::Debug) {
      std::cerr << "[ShapesAligner::computeDirect]| direct solver T: "
                << srrg_core::t2v(_direct_solver.transform()).transpose() << std::endl;
    }
  }


  void ShapesAligner::_computeIterative(const Isometry3& init_guess_) {
    double t0 = 0;
    _iterative_solver.init(init_guess_, _configuration.use_support_weight);
    _iterative_solver.setIgnoreOutliers(IGN_OL);

    for (int i=0; i < _configuration.max_iterations; ++i) {
      _iterative_solver.constraints().clear();

      t0 = srrg_core::getTime();
      _ass->compute(_iterative_solver.constraints(), _iterative_solver.transform());
      _stats.data_association += srrg_core::getTime() - t0;

      t0 = srrg_core::getTime();
      //_iterative_solver.setIgnoreOutliers(i==_configuration.max_iterations-1);
      _iterative_solver.setIgnoreOutliers(i>_configuration.max_iterations*3/4);
	  /** adaptive damping **/
	//  _iterative_solver.setDamping(
	//		  i>_configuration.max_iterations*3/4
	//		  ? 1.0
	//		  : 0.4);
      _iterative_solver.oneRound();
      _stats.solve += srrg_core::getTime() - t0;

      if(_configuration.verbosity == VerbosityLevel::Debug && (!i || i == _configuration.max_iterations-1)){
        std::cerr << "[[ShapesAligner::computeIterative]|]| it: " << i << " "
                  << "chi square: " << _iterative_solver.chiSquare() << " "
                  << "num inliers: " << _iterative_solver.numInliers() << "   "
                  << "T: " << srrg_core::t2v(_iterative_solver.transform()).transpose() << std::endl;
      }
    }

  }

  
  void ShapesAligner::compute(const Isometry3& init_guess_) {
    
    if(!_map_fixed || !_scene_moving)
      throw std::runtime_error("[ALIGNER][compute]: null pointers");

    double t0 = 0;
    _stats.setZero();
    _ass->setScenes(_map_fixed, _scene_moving);
    _ass->init();

    if(_configuration.use_direct_solver){
      _computeDirect(init_guess_);
      _T = _direct_solver.transform();

      //ia update tracks on the basis of final constraints configuration for DIRECT solver
      for (size_t i = 0; i < _direct_solver.constraints().size(); ++i) {
        const Constraint& c = _direct_solver.constraints()[i];
        assert(c.fixed && c.moving && "[ShapesAligner] unexpected error");
        c.moving->connectSceneEntry(c.fixed);
      }
      
    } else  {
      _computeIterative(init_guess_);
      _T = _iterative_solver.transform();

      //ia update tracks on the basis of final constraints configuration for ITERATIVE solver
      for (size_t i = 0; i < _iterative_solver.constraints().size(); ++i) {
        const Constraint& c = _iterative_solver.constraints()[i];
        //
        //        std::cerr << "constraint: fixed = " << FG_BCYAN((size_t)c.fixed)
        //                  << "\tmoving = " << FG_BCYAN((size_t)c.moving) << std::endl;

        assert(c.fixed && c.moving && "[ShapesAligner] unexpected error");
        c.moving->connectSceneEntry(c.fixed);
        //        if (c.fixed->landmark()) {
        //          if (c.fixed->landmark()->origin()->landmark() != c.fixed->landmark())
        //            throw std::runtime_error("aligner disgrazie");
        //        }

      }
    }


    if(_configuration.verbosity >= VerbosityLevel::Time){
      std::cerr <<  _stats << std::endl;
    }
    //std::cerr << "Final T: " << srrg_core::t2v(_T).transpose() << std::endl;
  }

} //ia end namespace srrg_bagasha
