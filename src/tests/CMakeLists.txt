add_executable(test_shared_ptr test_shared_ptr.cpp)
target_link_libraries(test_shared_ptr
  srrg_sashago_types_library
  ${catkin_LIBRARIES})

add_executable(test_matchables test_matchables.cpp)
target_link_libraries(test_matchables
  srrg_sashago_types_library
  ${catkin_LIBRARIES})
  
add_executable(test_aligner test_aligner.cpp)
target_link_libraries(test_aligner
  srrg_sashago_types_library
  srrg_sashago_aligner_library
  ${catkin_LIBRARIES})

add_executable(test_direct_solver test_direct_solver.cpp)
target_link_libraries(test_direct_solver
  srrg_sashago_types_library
  srrg_sashago_aligner_library
  ${catkin_LIBRARIES})
    
add_executable(test_map_viewer test_map_viewer.cpp)
target_link_libraries(test_map_viewer
  srrg_sashago_viewer_library
  ${catkin_LIBRARIES})
  

