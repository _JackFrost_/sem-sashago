#!/usr/bin/python3

import sys
import os

# Marks at the start of timestamp file, indicating
# that an association step took place before TXTIOfication
# By default R indicates rgb, D indicates depth
rd_marks = {'R' : 'rgb', 'D' : 'depth'}

def getTsFromFileName(fileName, delim='/'):
    """
    Assumes: .../<timestamp>.<extension>
    """
    path_parts = fileName.split(delim)
    return os.path.splitext(path_parts[-1])[0]

def printDictProps(tar_dict, name):
    print("{} props:".format(name))
    print("#Keys={:}, #Values={:}, #UVals={:}"
            .format(len(list(tar_dict.keys())),
                len(list(tar_dict.values())),
                len(set(tar_dict.values()))))

def getDicts(tsFile):
    """
    Basically read a TUM-compliant ts-assoc file
    Returns two dicts, one for RGB and one for Depth images.
    Dict format:
        <float timestamp> => <int index>
    """
    rgb_dict = {}
    depth_dict = {}
    with open(tsFile, 'r') as fin:
        for line in fin:
            lineData = line.split()
            if (rd_marks[lineData[0]] == 'depth'):
                depth_dict[float(lineData[2])] = int(lineData[1])
            else:
                rgb_dict[float(lineData[2])] = int(lineData[1])

    printDictProps(rgb_dict, "RGB")
    printDictProps(depth_dict, "Depth")
    return rgb_dict, depth_dict

def parseDirectory(imgDirs, topicStrs, tsFile, tarDir=None, prevDelim='/', newDelim='.', numFmtStr="_{:08}"):
    dicts = getDicts(tsFile)
    dir_cnt = -1
    for imgDir, topicStr in zip(imgDirs, topicStrs):
        dir_cnt += 1
        if (imgDir[-1] == '/'):
            imgDir = imgDir[:-1]
        if (topicStr[0] == '/'):
            topicStr = topicStr[1:]

        if (not tarDir):
            tarDir = imgDir
        elif (tarDir[-1] == '/'):
            tarDir = tarDir[:-1]

        img_dir = os.fsencode(imgDir)
        topicStr = topicStr.replace(prevDelim, newDelim)

        with open("img_log_{:}.txt".format(dir_cnt), 'w') as flog:
            cnt = 0
            for fimg in sorted(os.listdir(img_dir)):
                img_old_base = os.fsdecode(fimg)
                ts = float(os.path.splitext(img_old_base)[0])
                dict_idx = 0 if "rgb" in topicStr else 1
                if (ts not in dicts[dict_idx].keys()):
                    continue
                oldName = imgDir+'/'+img_old_base
                newName = tarDir+'/'+topicStr+numFmtStr.format(cnt+4)+os.path.splitext(oldName)[1]
                flog.write(oldName + '\t' + newName + '\n')
                #print(oldName + '\t' + newName)
                #os.rename(oldName, newName)
                cnt += 1
     


if __name__=='__main__':
    cnt = (len(sys.argv)-3) // 2
    dirs = sys.argv[1:cnt+1]
    topics = sys.argv[cnt+1:2*cnt+1]
    tsFile = sys.argv[2*cnt+1]
    tarDir = sys.argv[2*cnt+2]

    parseDirectory(dirs, topics, tsFile, tarDir)


