#!/usr/bin/python3

import os
import glob
import argparse

# Marks at the start of timestamp file, indicating
# that an association step took place before TXTIOfication
# By default R indicates rgb, D indicates depth
rd_marks = {'rgb' : 'R', 'depth' : 'D'}

# Calibration params
rgb_fr1 = [517.3, 0, 318.6, 0, 516.5, 255.3]
depth_fr1 = [591.1, 0, 331.0, 0, 590.1, 234.0]
ros_default = [525.0, 0, 319.5, 0, 525.0, 239.5]
intnet_default = [600, 0, 320, 0, 600, 240]
icl_default = [481.2, 0, 319.5, 0, -480, 239.5]

### EDIT THIS LINE ###
calibration_data = intnet_default
######################


def getTsFromFileName(fileName, delim='/'):
    """
    Assumes: .../<timestamp>.<extension>
    """
    path_parts = fileName.split(delim)
    return os.path.splitext(path_parts[-1])[0]

def getTsFromAssocfile(filename, seqNum, dirSpec=None):
    with open(filename, 'r') as fin:
        for line in fin:
            lineData = line.split()
            if (dirSpec):   # For TUM-like format, where assoc file generated timestamps
                if (rd_marks[dirSpec] != lineData[0]):
                    continue
                idx = int(lineData[1])
                if (idx == seqNum):
                    return lineData[2].strip()
            else:
                idx = int(lineData[0])
                if idx == seqNum:
                    return lineData[1].strip()

def writeLine(
        outFile,
        topic,
        seqNum,
        timestamp,
        distParams,
        fileName,
        calibVector,
        msgType = "PINHOLE_IMAGE_MESSAGE",
        frameType = "/openni_rgb_optical_frame",
        seqNumFmtString = "{:08}"):
    line = ' '.join([
        msgType,
        topic,
        frameType,
        seqNumFmtString.format(seqNum),
        str(timestamp)] +
        [str(d) for d in distParams] +
        [fileName] +
        [str(c) for c in calibVector])
    outFile.write(line+'\n')


ignoreDict = ['\"', '\'', '#', '\\']
def line2parts(line, delim=None):
    """
    Split up a line into parts, unless it's a comment
    """
    if (line.strip()[0] in ignoreDict):
        return None
    return [w for w in line.strip().split(delim)]

def getOdom(odomFile):
    if (not odomFile):
        return None

    with open(odomFile, 'r') as fin:
        line_data = list(filter(None, [line2parts(line) for line in fin]))
        return {float(line[0]) : line for line in line_data }

def parseDirectory(outFile, rgbDir, depthDir=None, tarDir="", rgbReplace='rgb', depthReplace='depth', tsFile=None, assocPresent=False, odomFile=None, inds=[0,-1]):
    """
    Receives an output file handler to write out the TXTIO messages,
    along with system directories that are to be iterated, so that
    the list of files may be generated. If depthDir == None, then
    the assumption is that if "rgb" is substituted by "depth" in the
    rgbDir path, the depth path is received.
    Also adds odometry information if odomFile isn't None.
    """

    odom_dict = getOdom(odomFile)

    #rgb_dir = os.fsencode(rgbDir)
    #if (not depthDir):
    #    depth_dir = os.fsencode(rgbDir.replace(rgbReplace, depthReplace))
    #else:
    #    depth_dir = os.fsencode(depthDir)

    #for cnt, (frgb, fdepth) in enumerate(zip(sorted(os.listdir(rgb_dir)), sorted(os.listdir(depth_dir)))):
    rgbList = sorted(glob.glob(rgbDir+'/*rgb*'))
    depthList = sorted(glob.glob(rgbDir+'/*depth*'))
    if (inds[1] == -1):
        inds[1] = len(rgbList)
    for cnt, (frgb, fdepth) in enumerate(zip(rgbList, depthList)):
        if cnt < inds[0]:
            continue
        if cnt >= inds[1]:
            break
        seqNum = cnt + 4
        filename = os.fsdecode(fdepth)
        timestamp = getTsFromFileName(filename) if (not tsFile) \
            else getTsFromAssocfile(tsFile, seqNum, dirSpec=("depth" if assocPresent else None))
        rfilename = os.fsdecode(frgb)
        rtimestamp = getTsFromFileName(rfilename) if (not tsFile) \
            else getTsFromAssocfile(tsFile, seqNum, dirSpec=("rgb" if assocPresent else None))
        
        print(timestamp)
        print(rtimestamp)

        odom_data = [0] if (not odom_dict) else [1]+odom_dict[float(rtimestamp)][1:]

        writeLine(
                outFile,
                "/camera/depth/image",
                seqNum,
                timestamp,
                [0,0,0,0,0,0]+odom_data+[0,0.001],
                (tarDir+'/' if len(tarDir) > 0 else '') + filename,
                calibration_data)
        
        writeLine(
                outFile,
                "/camera/rgb/image_color",
                seqNum,
                rtimestamp,
                [0,0,0,0,0,0]+odom_data+[0, 0.001],
                (tarDir+'/' if len(tarDir) > 0 else '') + rfilename,
                calibration_data)


if __name__=='__main__':
    #argc = 1
    #
    #path = sys.argv[argc]
    #argc += 1

    #outFile = "output.txtio"
    #if (len(sys.argv) > argc):
    #    outFile = sys.argv[argc]
    #argc += 1

    #tarDir = ''
    #if (len(sys.argv) > argc):
    #    tarDir = sys.argv[argc]
    #argc += 1

    #tsFile = None
    #if (len(sys.argv) > argc):
    #    tsFile = sys.argv[argc]
    #argc += 1

    #odomFile = None
    #if (len(sys.argv) > argc):
    #    odomFile = sys.argv[argc]
    #argc += 1

    parser = argparse.ArgumentParser(description='''
    This script generates a txtio file given the necessary data
    ''')

    # File arguments
    parser.add_argument('path', help='the .txtio.d directory')
    parser.add_argument('file_out', help='output .txtio file')
    parser.add_argument('--tar_dir', default='', required=False, help='target directory, in case the images are not in their final destination folder yet')
    parser.add_argument('--ts_file', default=None, required=False, help='timestamp file')
    parser.add_argument('--odom_file', default=None, required=False, help='odometry file, txtio odometry format [x y z qx qy qz], quaternions normalized and with qw>0')

    # Sequence limits
    parser.add_argument('--idx_start', type=int, required=False, default=0, help="Which should be the first (inclusive) index to compute [default: 0]")
    parser.add_argument('--idx_end', type=int, required=False, default=-1, help="Which should be the last (exclusive) index to compute [default: -1, which means the last one available]")

    # Switches
    #parser.add_argument('--absolute', '-a', dest='absolute', action='store_true', help='Indicate that output frames are in absolute coordinates of the initial frame (by default they are relative to the previous frame in the sequence)')
    #parser.set_defaults(absolute=False)
    #parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debugging output')
    #parser.set_defaults(debug=False)

    args = parser.parse_args()

    outFile = args.file_out
    path = args.path
    tsFile = args.ts_file
    odomFile = args.odom_file
    tarDir = args.tar_dir
    idc = [args.idx_start, args.idx_end]

    with open(outFile, 'w') as fout:
        print(path)
        print(outFile)
        print(tsFile)
        print(odomFile)
        parseDirectory(fout, path, tarDir=tarDir, tsFile=tsFile, assocPresent=False, odomFile=odomFile, inds=idc)
