#!/home/babyrobot/giannis/detectron/bin/python

import argparse
import sys
import os
import cv2
import numpy as np

# [params] - error_{pre, post}_opt [U5_fr1_config, sema_weight 0.25, no hdn]
mfp = 5 # kernel window size

def parse_directory(depthDir, outDir):
    if (depthDir[-1] == '/'):
        depthDir = depthDir[:-1]
    if (outDir[-1] == '/'):
        outDir = outDir[:-1]

    depthList = os.fsencode(depthDir)
    cnt = 0
    fld = sorted(os.listdir(depthList))
    for fd in fld:
        depthName = os.fsdecode(fd)
        print(depthDir+'/'+depthName)

        # Read using anydepth flag, for TUM images this defaults to 16-bit images
        oldDepth = cv2.imread(depthDir+'/'+depthName, cv2.IMREAD_ANYDEPTH)
        newDepth = cv2.medianBlur(oldDepth, mfp)
        cv2.imwrite(outDir+'/'+depthName, newDepth)
        print(outDir+'/'+depthName)
        cnt += 1
        print("Done with {:} out of {:}".format(cnt, len(fld)))
    
    print("Filtered {:} image{} in total.".format(cnt, ('s' if cnt != 1 else '') ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script applies median filtering to images in a directory.
    ''')
    parser.add_argument('depth_folder', help='input depth folder (image format: png)')
    #parser.add_argument('intensity_thresh', help='cutoff value below which depths will be suppressed (default=20)', type=int, default=20)
    parser.add_argument('output_folder', help='destination depth folder')
    args = parser.parse_args()

    parse_directory( \
            args.depth_folder, \
            args.output_folder)

