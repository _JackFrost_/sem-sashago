#!/usr/bin/python

## Imports

import argparse


## Constants

# Uncomment this line in case class IDs are not known
#class_ids = None

# If you know then this might be good
# FasterRCNN classes from COCO: https://gist.github.com/AruniRC/7b3dadd004da04c80198557db5da4bda
#   NOTE: In fact the output is skewed by 1, namely:
#       output_class = COCO_code - 1
#       therefore the input file should contain for example code 62 for TV (even though it's 63 in the link above)
num_classes = 81 
class_ids = list(range(num_classes))
cl_offset = 100

## Functions

def readBatchFile(fileName):
    """
    Reads in the batching data
    Each line of the batching file
    should contain the class IDs that
    must be considered synonymous
    Returns a list of lists containing batch data
    """
    with open(fileName, 'r') as fin:
        ret = [list(map(int, line.strip().split())) for line in fin]

        # Sanity check
        tot = sum([len(r) for r in ret])
        tot_s = set()
        for r in ret:
            for el in r:
                tot_s.add(el)
        assert tot == len(tot_s), "Duplicate classes detected in synonym set: {} VS {}".format(str(tot), str(tot_s))

        return ret

def readDelFile(fileName):
    """
    Reads the deletion file, containing class IDs,
    no specific format required, so long as split()
    can separate them
    """
    with open(fileName, 'r') as fin:
        data = [line.strip().split() for line in fin]
        ret = []
        for d in data:
            for elem in d:
                if not elem:    # check for empty strings, just in case
                    continue
                ret.append(elem)
        return ret

def findGroup(clid, syn):
    """
    Return the synonym index if it exists for clid
    Else return clid
    """
    for i in range(len(syn)):
        if clid in syn[i]:
            return max(class_ids)+cl_offset+i
    return clid

def convertDetections(file_in, file_out, synonyms, purge):
    """
    Reads in the detection file
    Format:
        0   1     2     3     4     5        6
        ts [bbx1, bby1, bbx2, bby2] class_id score

    Output in the same format, but class_id will change
    in case it belongs to some batching entry or deleted if
    they belong to purge
    """
    if not class_ids:
        raise Exception("Not implemented yet")

    with open(file_in, 'r') as fin,\
        open(file_out, 'w') as fout:
        data = [line.strip().split() for line in fin]
        for d in data:
            if (len(d) < 7):
                continue
            if (purge and d[5] in purge):
                continue
            d[5] = str(findGroup(int(d[5]), synonyms))
            fout.write(' '.join(d)+'\n')


## Driver code

def main(args):
    file_in = args.file_in
    file_out = args.file_out
    file_syn = args.file_syn
    file_del = args.file_del

    synonyms = readBatchFile(file_syn)
    purge = readDelFile(file_del) if file_del else None
    convertDetections(file_in, file_out, synonyms, purge)

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='''
        Takes a semantic detection file as input, along with
        a semantic class equivalence list file and an optional class
        suppression file. Outputs a semantic
        detection file where classes that belong to equivalence groups
        are replaced by new, common group IDs and suppressed classes
        are completely removed.
    ''')

    parser.add_argument('file_in', help='the input semantic file')
    parser.add_argument('file_syn', help='the class synonyms file')
    parser.add_argument('file_out', help='the output semantic file')

    parser.add_argument('--file_del', '-d', help='the class deletion file')
    main(parser.parse_args())

















