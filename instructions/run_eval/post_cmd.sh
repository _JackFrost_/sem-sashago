#!/bin/bash

if (( $# == 0 )); then
	echo "No arguments present, give target directory."
	exit 1
fi
curDir=$(pwd)
cd "$1"
rm *.sashago
cp $curDir/*.sashago .

sep="------------------"
echo $sep
echo "Pre-optimization:"
./eval_cmd.sh tum_gt.txt tum_track.sashago preopt
echo $sep
echo "Post-optimization:"
./eval_cmd.sh tum_gt.txt tum_track_2.sashago
