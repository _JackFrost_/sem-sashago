#!/bin/bash

xhost +
ds_mount=''
if (( $# == 2 )); then
    ds_mount="--mount type=bind,source=$2,target=/dataset"
elif (( $# != 1 )); then
    echo "Unexpected number of arguments, expected a docker image and an optional volume mount point."
    echo "Available images are:"
    sudo docker image ls
    #echo "Available volumes are:"
    #sudo docker volume ls
fi

echo "Launching full-NVIDIA docker image: $1"

sudo docker run \
    --runtime=nvidia -it --rm \
    -e DISPLAY="$DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    $ds_mount $1
